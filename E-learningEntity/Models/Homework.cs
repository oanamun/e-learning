﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Homework
    {
        public Homework()
        {
            this.Grades = new HashSet<Grade>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Format { get; set; }
        public System.DateTime? End_date { get; set; }
        public string Grading_type { get; set; }
        public bool Required { get; set; }

        [Range(0, 1)]
        public float? Pondere { get; set; }

        [ForeignKey("Module")]
        public int? module_id { get; set; }
        public virtual Module Module { get; set; }

        [ForeignKey("Group")]
        public int? group_id { get; set; }
        public virtual Group Group { get; set; }

        public ICollection<Grade> Grades { get; set; }
    }
}