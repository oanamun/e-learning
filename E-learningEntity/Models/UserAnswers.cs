﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class UserAnswers
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int? user_Id { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("AnswerOptions")]
        public int answerOptionId { get; set; }
        public virtual AnswerOptions AnswerOptions { get; set; }
    }
}