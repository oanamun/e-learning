﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Discussion
    {
        public Discussion()
        {
            this.Comments = new HashSet<Comment>();
            this.Grades = new HashSet<Grade>();
        }

        public int Id { get; set; }
        [Required]
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        [ForeignKey("Course")]
        public int? course_id { get; set; }
        public virtual Course Course { get; set; }
     

        [ForeignKey("Group")]
        public int? group_id { get; set; }
        public virtual Group Group { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<Grade> Grades { get; set; }
    }
}