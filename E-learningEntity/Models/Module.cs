﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Module
    {
        public Module()
        {
            this.Homework = new HashSet<Homework>();
            this.Tests = new HashSet<Test>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public System.DateTime? Start_date { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }

        [ForeignKey("Course")]
        public int course_id { get; set; }
        public virtual Course Course { get; set; }

        public ICollection<Homework> Homework { get; set; }
        public ICollection<Test> Tests { get; set; }        
    }
}