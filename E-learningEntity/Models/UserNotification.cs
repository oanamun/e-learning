﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class UserNotification
    {
        public int Id { get; set; }
        public bool Seen { get; set; }
        public string Message { get; set; }

        [ForeignKey("User")]
        public int user_id { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Notification")]
        public int? notification_id { get; set; }
        public virtual Notification Notification { get; set; }       
    }
}