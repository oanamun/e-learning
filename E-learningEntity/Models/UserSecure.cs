﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class UserSecure
    {
        public int Id { get; set; }
        public string Username { get; set; }

        public string Salt { get; set; }
        public string Hash { get; set; }
    }
}