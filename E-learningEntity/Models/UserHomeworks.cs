﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class UserHomeworks
    {

        public int Id { get; set; }
        public string Answer { get; set; }

        [ForeignKey("Homework")]
        public int homework_id { get; set; }
        public virtual Homework Homework { get; set; }

        [ForeignKey("User")]
        public int? user_id{get;set;}
        public virtual User User {get;set;}
    }
}