﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Logs
    {
        public Logs() { }
        public int Id { get; set; }
        [Required]
        public System.DateTime Date { get; set; }

        [ForeignKey("User")]
        public int user_id { get; set; }
        public virtual User User { get; set; }
    }
}