﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace E_learningEntity.Models
{
    public class AnswerOptions
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set;}
        [Required]
        public bool Correct { get; set; }
        
        [ForeignKey("Question")]
        public int question_id {get;set;}
        public virtual Question Question {get;set;}
    }
}