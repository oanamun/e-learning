﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string Description { get; set; }

        [Range(0,1)]
        public float? Pondere { get; set; }

        [ForeignKey("Test")]
        public int test_id { get; set; }
        public virtual Test Test { get; set; }

        public ICollection<AnswerOptions> Choices { get; set; }
       
    }
}