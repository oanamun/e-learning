﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_learningEntity.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Email { get; set; }
        public bool Gender { get; set; }
        public int? Registration_number { get; set; }
        public bool? Suspended { get; set; }
        public bool? Blocked { get; set; }
        public bool Student { get; set; }
        public DateTime? SuspendedUntil { get; set; }

        public string Salt { get; set; }

        [ForeignKey("Group")]
        public int? group_id { get; set; }
        public virtual Group Group { get; set; }

        public ICollection<Permission> Permissions { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<UserConversation> UserConversations { get; set; }
        public ICollection<UserNotification> UserNotifications { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Page> Pages { get; set; }
        public ICollection<Grade> Grades { get; set; }
        public ICollection<Logs> Logs { get; set; }

    }
}