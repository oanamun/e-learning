﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace E_learningEntity.Models
{
    public class Event
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public System.DateTime? End_date { get; set; }

        [ForeignKey("User")]
        public int? user_id { get; set; }
        public virtual User User { get; set; }
    }
}