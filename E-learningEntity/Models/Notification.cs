﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Notification
    {
        public Notification()
        {
            this.UserNotifications = new HashSet<UserNotification>();
        }

        public int Id { get; set; }
        [Required]
        public string Message { get; set; }

        [ForeignKey("Course")]
        public int? course_id { get; set; }
        public Course Course { get; set; }

        public ICollection<UserNotification> UserNotifications { get; set; }
       
    }
}