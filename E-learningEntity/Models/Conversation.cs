﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Conversation
    {
        public Conversation()
        {
            this.UserConversations = new HashSet<UserConversation>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<UserConversation> UserConversations { get; set; }
    }
}