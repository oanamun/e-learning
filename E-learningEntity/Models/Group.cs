﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Group
    {
        public Group()
        {
            this.Users = new HashSet<User>();
            this.Announcements = new HashSet<Announcement>();
            this.Discussions = new HashSet<Discussion>();
            this.Pages = new HashSet<Page>();
            this.Homework = new HashSet<Homework>();
            this.Tests = new HashSet<Test>();
        }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [ForeignKey("Course")]
        public int? course_id { get; set; }
        public virtual Course Course { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<Announcement> Announcements { get; set; }
        public ICollection<Discussion> Discussions { get; set; }
        public ICollection<Page> Pages { get; set; }
        public ICollection<Homework> Homework { get; set; }
        public ICollection<Test> Tests { get; set; }
    }
}