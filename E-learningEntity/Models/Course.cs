﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Course
    {
        public Course()
        {
            Permissions = new HashSet<Permission>();
            Modules = new HashSet<Module>();
            Discussions = new HashSet<Discussion>();
            Announcements = new HashSet<Announcement>();
            Grades = new HashSet<Grade>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public float? PondereTests { get; set; }
        public float? PondereHomeworks { get; set; }
        public float? PondereDiscussions { get; set; }
        public DateTime End_date { get; set; }
        public DateTime Start_date { get; set; }

        public ICollection<Permission> Permissions { get; set; }
        public ICollection<Module> Modules { get; set; }
        public ICollection<Discussion> Discussions { get; set; }
        public ICollection<Announcement> Announcements { get; set; }
        public ICollection<Grade> Grades { get; set; }
    }
}