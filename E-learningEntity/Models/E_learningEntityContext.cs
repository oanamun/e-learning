﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class E_learningEntityContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public E_learningEntityContext() : base("name=E_learningEntityContext")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

        }

        public System.Data.Entity.DbSet<E_learningEntity.Models.Group> Groups { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Conversation> Conversations { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.User> Users { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Announcement> Announcements { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Course> Courses { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Comment> Comments { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Discussion> Discussions { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Event> Events { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Grade> Grades { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Homework> Homeworks{ get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Logs> Logs { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Message> Messages { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Module> Modules { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Notification> Notifications { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Page> Pages { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Permission> Permissions { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Question> Questions { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Test> Tests { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.UserConversation> UserConversations { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.UserNotification> UserNotifications { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.Token> Tokens { get; set; }

        public System.Data.Entity.DbSet<E_learningEntity.Models.AnswerOptions> AnswerOptions { get; set; }

        public System.Data.Entity.DbSet<E_learningEntity.Models.UserAnswers> UserAnswers { get; set; }

        public System.Data.Entity.DbSet<E_learningEntity.Models.UserHomeworks> UserHomeworks { get; set; }
        public System.Data.Entity.DbSet<E_learningEntity.Models.UserSecure> UserSercured { get; set; }
    }
}
