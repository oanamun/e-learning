﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Grade
    {
        public int Id { get; set; }
        [Range(0,10)]
        [Required]
        public float Value { get; set; }

        [ForeignKey("User")]
        public int user_id { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Discussion")]
        public int? discussion_id { get; set; }
        public virtual Discussion Discussion { get; set; }

        [ForeignKey("Course")]
        public int? course_id { get; set; }
        public virtual Course Course { get; set; }

        [ForeignKey("Homework")]
        public int? homework_id { get; set; }
        public virtual Homework Homework { get; set; }

        [ForeignKey("Test")]
        public int? test_id { get; set; }
        public virtual Test Test { get; set; }
    }
}