﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class UserConversation
    {
        public UserConversation()
        {
            this.Messages = new HashSet<Message>();
        }

        public int Id { get; set; }

        [ForeignKey("User")]
        public int? user_id { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Conversation")]
        public int conversation_id { get; set; }
        public virtual Conversation Conversation { get; set; }


        public ICollection<Message> Messages { get; set; }
        

    }
}