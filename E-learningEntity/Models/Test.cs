﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Test
    {
        public Test()
        {
            this.Questions = new HashSet<Question>();
            this.Grades = new HashSet<Grade>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public System.DateTime? Due_date { get; set; }
        public string Type { get; set; }

        [Range(0, 1)]
        public float? Pondere { get; set; }

        [ForeignKey("Module")]
        public int? module_id { get; set; }
        public virtual Module Module { get; set; }

        public ICollection<Question> Questions { get; set; }
        public ICollection<Grade> Grades { get; set; }
    
    }
}