﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace E_learningEntity.Services
{
        /// <summary>
        /// Offers services for user specific operations
        /// </summary>
        public class UserService : IUserService
        {
            private readonly UnitOfWork _unitOfWork;

            /// <summary>
            /// Public constructor.
            /// </summary>
            public UserService(UnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            /// <summary>
            /// Public method to authenticate user by user name and password.
            /// </summary>
            /// <param name="userName"></param>
            /// <param name="password"></param>
            /// <returns></returns>
            public int Authenticate(string userName, string password)
            {
            //var user = _unitOfWork.UserRepository.Get(u => u.Username == userName && u.Password == password);
            try {
                string salt = GetSalt(userName);
                if (salt != null)
                {
                    var saltedPassword = GetFinal(password, salt);
                    var user = _unitOfWork.UserRepository.Get(u => u.Username == userName && u.Password == saltedPassword);
                    if (user != null && user.Id > 0)
                    {
                        return user.Id;
                    }
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
            }
            return 0;
            }

            public string GetSalt(string username)
            {
                    string salt= _unitOfWork.UserRepository.Get(obj => obj.Username == username).Salt;
            if (salt == null)
                return null;
            else return salt;
            }

            public static byte[] ComputeHash(string plainText)
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                HashAlgorithm hash = new SHA256Managed();
                return hash.ComputeHash(plainTextBytes);
            }

            public static string GetFinal(string password,string salt)
            {
                byte[] passwordHash = ComputeHash(password);
                byte[] saltHash = ComputeHash(salt);

                byte[] hashWithSaltBytes = new byte[passwordHash.Length + saltHash.Length];
                for (int i = 0; i < passwordHash.Length; i++)
                    hashWithSaltBytes[i] = passwordHash[i];
                for (int i = 0; i < saltHash.Length; i++)
                    hashWithSaltBytes[passwordHash.Length + i] = saltHash[i];
                return Convert.ToBase64String(hashWithSaltBytes);
            }

            static string HashPassword(string pasword)
            {
                byte[] arrbyte = new byte[pasword.Length];
                SHA256 hash = new SHA256CryptoServiceProvider();
                arrbyte = hash.ComputeHash(Encoding.UTF8.GetBytes(pasword));
                return Convert.ToBase64String(arrbyte);
            }

        }
    }