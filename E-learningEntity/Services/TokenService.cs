﻿using E_learningEntity.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace E_learningEntity.Services
{
    public class TokenServices : ITokenServices
    {
        #region Private member variables.
        private readonly UnitOfWork _unitOfWork;
        #endregion

        #region Public constructor.
        /// <summary>
        /// Public constructor.
        /// </summary>
        public TokenServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion


        #region Public member methods.

        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Token GenerateToken(int userId)
        {
            string token = Guid.NewGuid().ToString();
            DateTime issuedOn = DateTime.Now;
            DateTime expiredOn = DateTime.Now.AddSeconds(
                                              Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
            var tokendomain = new Token
            {
                UserId = userId,
                AuthToken = token,
                IssuedOn = issuedOn,
                ExpiresOn = expiredOn
            };

            _unitOfWork.TokenRepository.Insert(tokendomain);
            _unitOfWork.Save();
            var tokenModel = new Token()
            {
                UserId = userId,
                IssuedOn = issuedOn,
                ExpiresOn = expiredOn,
                AuthToken = token
            };

            return tokenModel;
        }

        /// <summary>
        /// Method to validate token against expiry and existence in database.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public bool ValidateToken(string tokenId)
        {
            var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
            if (token != null && !(DateTime.Now > token.ExpiresOn))
            {
                token.ExpiresOn = token.ExpiresOn.AddSeconds(
                                              Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
                _unitOfWork.TokenRepository.Update(token);
                _unitOfWork.Save();
                return true;
            }
            return false;
        }

        //Check if the user is an admin based ons token
        public bool ValidateTokenAdmin(string tokenId)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                    //var permission = _unitOfWork.PermissionRepository.Context.Permissions.SingleOrDefault(perm=>perm.user_id==user_id);
                     var permission = _unitOfWork.PermissionRepository.GetSingle(perm=>perm.user_id==user_id && perm.Role.Equals("Admin"));
                    if (permission != null)
                        return true;
                }
                return false;
            }catch (Exception ) {
                return false;
            }
        }

        public bool ValidateCoursePermissions(string tokenId, int course_id)
        {
            if (ValidateTokenAssistantForCourse(tokenId, course_id) || ValidateTokenStudentForCourse(tokenId, course_id) || ValidateTokenTeacherForCourse(tokenId, course_id))
                return true;
            return false;
        }

        //Verifica daca un user este inscris la un anumit curs
        public bool ValidateTokenStudentForCourse(string tokenId, int course_id)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                     var permission = _unitOfWork.PermissionRepository.Context.Permissions.SingleOrDefault(perm=>perm.user_id==user_id && perm.course_id==course_id && perm.Role.Equals("Student"));
                    //var permission = _unitOfWork.PermissionRepository.GetSingle(perm => perm.User.Id == user_id && perm.Role.Equals("Admin"));
                    if (permission != null)
                        return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Verifica daca un user este profesorul de la curs
        public bool ValidateTokenTeacherForCourse(string tokenId, int course_id)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                    var permission = _unitOfWork.PermissionRepository.Context.Permissions.SingleOrDefault(perm => perm.user_id == user_id && perm.course_id == course_id && perm.Role.Equals("Profesor"));
                    //var permission = _unitOfWork.PermissionRepository.GetSingle(perm => perm.User.Id == user_id && perm.Role.Equals("Admin"));
                    if (permission != null)
                        return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Verifica daca un user este profesor
        public bool ValidateTokenTeacher(string tokenId)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                    User user = _unitOfWork.UserRepository.GetByID(user_id);
                    if (user.Student == false)
                        return true;
                    else
                        return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Verifica daca un user este student
        public bool ValidateTokenStudent(string tokenId)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                    User user = _unitOfWork.UserRepository.GetByID(user_id);
                    if (user.Student == true)
                        return true;
                    else
                        return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Verifica daca un user este asistent
        public bool ValidateTokenAssistant(string tokenId)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                    var permission = _unitOfWork.PermissionRepository.Context.Permissions.SingleOrDefault(perm => perm.user_id == user_id && perm.Role.Equals("Asistent"));
                    if (permission != null)
                        return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Verifica daca un user este asistent de la curs
        public bool ValidateTokenAssistantForCourse(string tokenId, int course_id)
        {
            try
            {
                var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
                if (token != null)
                {
                    var user_id = token.UserId;
                    var permission = _unitOfWork.PermissionRepository.Context.Permissions.SingleOrDefault(perm => perm.user_id == user_id && perm.course_id == course_id && perm.Role.Equals("Asistent"));
                    //var permission = _unitOfWork.PermissionRepository.GetSingle(perm => perm.User.Id == user_id && perm.Role.Equals("Admin"));
                    if (permission != null)
                        return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int GetUserIdByToken(string tokenId)
        {
            var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId);
            return token.UserId;
        }

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenId">true for successful delete</param>
        public bool Kill(string tokenId)
        {
            _unitOfWork.TokenRepository.Delete(x => x.AuthToken == tokenId);
            _unitOfWork.Save();
            var isNotDeleted = _unitOfWork.TokenRepository.GetMany(x => x.AuthToken == tokenId).Any();
            if (isNotDeleted) { return false; }
            return true;
        }

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true for successful delete</returns>
        public bool DeleteByUserId(int userId)
        {
            _unitOfWork.TokenRepository.Delete(x => x.UserId == userId);
            _unitOfWork.Save();

            var isNotDeleted = _unitOfWork.TokenRepository.GetMany(x => x.UserId == userId).Any();
            return !isNotDeleted;
        }

        #endregion
    }
}