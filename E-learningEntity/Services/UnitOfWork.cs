﻿using E_learningEntity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace E_learningEntity.Services
{
    // <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IDisposable
    {
        #region Private member variables...

        private E_learningEntityContext _context = null;
        private GenericRepository<User> _userRepository;
        private GenericRepository<Token> _tokenRepository;
        private GenericRepository<Permission> _permissionRepository;
        private GenericRepository<Discussion> _discussionRepository;
        private GenericRepository<UserConversation> _userConversationRepository;
        private GenericRepository<Conversation> _conversationRepository;
        private GenericRepository<Course> _courseRepository;
        private GenericRepository<Module> _moduleRepository;
        private GenericRepository<Announcement> _announcementRepository;
        private GenericRepository<Grade> _gradeRepository;
        private GenericRepository<Event> _eventRepository;
        private GenericRepository<Comment> _commentRepository;
        private GenericRepository<Page> _pageRepository;
        private GenericRepository<Notification> _notificationRepository;
        private GenericRepository<UserNotification> _userNotificationRepository;
        private GenericRepository<Logs> _logsRepository;
        private GenericRepository<Group> _groupRepository;
        private GenericRepository<Test> _testRepository;
        private GenericRepository<Homework> _homeworkRepository;
        private GenericRepository<Question> _questionRepository;
        private GenericRepository<AnswerOptions> _answerOptionsRepository;
        private GenericRepository<UserAnswers> _userAnswersRepository;
        private GenericRepository<Message> _messagesRepository;
        private GenericRepository<UserHomeworks> _userHwRepository;
        #endregion

        public UnitOfWork()
        {
            _context = new E_learningEntityContext();
        }

        #region Public Repository Creation properties...

  
        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public GenericRepository<User> UserRepository
        {
            get
            {
                if (this._userRepository == null)
                    this._userRepository = new GenericRepository<User>(_context);
                return _userRepository;
            }
        }

        public GenericRepository<Comment> CommentRepository
        {
            get
            {
                if (this._commentRepository == null)
                    this._commentRepository = new GenericRepository<Comment>(_context);
                return _commentRepository;
            }
        }

        public GenericRepository<UserHomeworks> UserHomeworkRepository
        {
            get
            {
                if (this._userHwRepository == null)
                    this._userHwRepository = new GenericRepository<UserHomeworks>(_context);
                return _userHwRepository;
            }
        }

        public GenericRepository<AnswerOptions> AnswerOptionsRepository
        {
            get
            {
                if (this._answerOptionsRepository == null)
                    this._answerOptionsRepository = new GenericRepository<AnswerOptions>(_context);
                return _answerOptionsRepository;
            }
        }

        public GenericRepository<Message> MessageRepository
        {
            get
            {
                if (this._messagesRepository == null)
                    this._messagesRepository = new GenericRepository<Message>(_context);
                return _messagesRepository;
            }
        }

        public GenericRepository<UserAnswers> UserAnswersRepository
        {
            get
            {
                if (this._userAnswersRepository == null)
                    this._userAnswersRepository = new GenericRepository<UserAnswers>(_context);
                return _userAnswersRepository;
            }
        }

        public GenericRepository<Question> QuestionRepository
        {
            get
            {
                if (this._questionRepository == null)
                    this._questionRepository = new GenericRepository<Question>(_context);
                return _questionRepository;
            }
        }

        public GenericRepository<Homework> HomeworkRepository
        {
            get
            {
                if (this._homeworkRepository == null)
                    this._homeworkRepository = new GenericRepository<Homework>(_context);
                return _homeworkRepository;
            }
        }

        public GenericRepository<Test> TestRepository
        {
            get
            {
                if (this._testRepository == null)
                    this._testRepository = new GenericRepository<Test>(_context);
                return _testRepository;
            }
        }

        public GenericRepository<Logs> LogsRepository
        {
            get
            {
                if (this._logsRepository == null)
                    this._logsRepository = new GenericRepository<Logs>(_context);
                return _logsRepository;
            }
        }

        public GenericRepository<Group> GroupRepository
        {
            get
            {
                if (this._groupRepository == null)
                    this._groupRepository = new GenericRepository<Group>(_context);
                return _groupRepository;
            }
        }

        public GenericRepository<UserNotification> UserNotificationRepository
        {
            get
            {
                if (this._userNotificationRepository == null)
                    this._userNotificationRepository = new GenericRepository<UserNotification>(_context);
                return _userNotificationRepository;
            }
        }

        public GenericRepository<Notification> NotificationRepository
        {
            get
            {
                if (this._notificationRepository == null)
                    this._notificationRepository = new GenericRepository<Notification>(_context);
                return _notificationRepository;
            }
        }

        public GenericRepository<Page> PageRepository
        {
            get
            {
                if (this._pageRepository == null)
                    this._pageRepository = new GenericRepository<Page>(_context);
                return _pageRepository;
            }
        }


        public GenericRepository<Course> CourseRepository
        {
            get
            {
                if (this._courseRepository == null)
                    this._courseRepository = new GenericRepository<Course>(_context);
                return _courseRepository;
            }
        }

        public GenericRepository<Event> EventRepository
        {
            get
            {
                if (this._eventRepository == null)
                    this._eventRepository = new GenericRepository<Event>(_context);
                return _eventRepository;
            }
        }

        public GenericRepository<Module> ModuleRepository
        {
            get
            {
                if (this._moduleRepository == null)
                    this._moduleRepository = new GenericRepository<Module>(_context);
                return _moduleRepository;
            }
        }

        public GenericRepository<Discussion> DiscussionRepository
        {
            get
            {
                if (this._discussionRepository == null)
                    this._discussionRepository = new GenericRepository<Discussion>(_context);
                return _discussionRepository;
            }
        }

        public GenericRepository<UserConversation> UserConversationRepository
        {
            get
            {
                if (this._userConversationRepository == null)
                    this._userConversationRepository = new GenericRepository<UserConversation>(_context);
                return _userConversationRepository;
            }
        }

        public GenericRepository<Conversation> ConversationRepository
        {
            get
            {
                if (this._conversationRepository == null)
                    this._conversationRepository = new GenericRepository<Conversation>(_context);
                return _conversationRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for permission repository.
        /// </summary>
        public GenericRepository<Permission> PermissionRepository
        {
            get
            {
                if (this._permissionRepository == null)
                    this._permissionRepository = new GenericRepository<Permission>(_context);
                return _permissionRepository;
            }
        }

        public GenericRepository<Announcement> AnnouncementRepository
        {
            get
            {
                if (this._announcementRepository == null)
                    this._announcementRepository = new GenericRepository<Announcement>(_context);
                return _announcementRepository;
            }
        }

        public GenericRepository<Grade> GradeRepository
        {
            get
            {
                if (this._gradeRepository == null)
                    this._gradeRepository = new GenericRepository<Grade>(_context);
                return _gradeRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for token repository.
        /// </summary>
        public GenericRepository<Token> TokenRepository
        {
            get
            {
                if (this._tokenRepository == null)
                    this._tokenRepository = new GenericRepository<Token>(_context);
                return _tokenRepository;
            }
        }
        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {

                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }

        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}