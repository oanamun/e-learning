﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;
using E_learningEntity.Services;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("messages")]
    public class MessagesController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Messages/5
        [Route("{id:int}")]
        [ResponseType(typeof(JObject))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> GetMessage(int id)
        {
            Message message = await db.Messages.FindAsync(id);
            if (message == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = message.Id;
            o["Text"] = message.Text;
            o["Date"] = message.Date;
            o["Username"] = message.UserConversation.User.Username;
            o["userConversation_id"] = message.userConversation_id;
            return Ok(o);
        }

        // PUT: api/Messages/5
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PutMessage(int id, Message message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != message.Id)
            {
                return BadRequest();
            }

            db.Entry(message).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Messages
        [ResponseType(typeof(JObject))]
        [HttpPost]
        [Route("")]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PostMessage(Message message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int userId = (int)ControllerContext.RouteData.Values["userId"];

            int userConv_id = _unitOfWork.UserConversationRepository.Get(conv => conv.user_id == userId && conv.conversation_id == message.userConversation_id).Id;
            message.userConversation_id =userConv_id;
            db.Messages.Add(message);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["Id"] = message.Id;
            o["Text"] = message.Text;
            o["Date"] = message.Date;
            o["Username"] =  db.Users.Find(userId).Username;
            o["userConversation_id"] = userConv_id;
            return Ok(o);
        }

        // DELETE: api/Messages/5
        [ResponseType(typeof(Message))]
        [AuthorizationRequired]
        [Route("id")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteMessage(int id)
        {
            Message message = await db.Messages.FindAsync(id);
            if (message == null)
            {
                return NotFound();
            }

            db.Messages.Remove(message);
            await db.SaveChangesAsync();

            return Ok(message);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MessageExists(int id)
        {
            return db.Messages.Count(e => e.Id == id) > 0;
        }        
    }
}