﻿using E_learningEntity.Filters;
using E_learningEntity.Models;
using E_learningEntity.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace E_learningEntity.Controllers
{
   [ApiAuthenticationFilter]
    public class LoginController : ApiController
    {
        private readonly ITokenServices _tokenServices;
        UnitOfWork _unitOfWork = new UnitOfWork();
        private E_learningEntityContext db = new E_learningEntityContext();

         public LoginController(ITokenServices tokenServices)
        {
            _tokenServices = tokenServices;
        }

        // POST api/Authenticate
         public HttpResponseMessage Authenticate()
         {
             if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
             {
                 var basicAuthenticationIdentity = System.Threading.Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                 if (basicAuthenticationIdentity != null)
                 {
                     var userId = basicAuthenticationIdentity.UserId;
                     return GetAuthToken(userId);
                 }
             }
             return null;
         }

         private HttpResponseMessage GetAuthToken(int userId)
         {
             try
             {
                 Token tokenAuth = _unitOfWork.TokenRepository.Context.Tokens.Single(tok => tok.UserId == userId);
                 if (!_tokenServices.ValidateToken(tokenAuth.AuthToken))
                 {
                     _unitOfWork.TokenRepository.Delete(tokenAuth.TokenId);
                     var token = _tokenServices.GenerateToken(userId);
                     var user = _unitOfWork.UserRepository.GetByID(userId);
                     JObject o = new JObject();
                     o["First_name"] = user.First_name;
                     o["Last_name"] = user.Last_name;
                     o["Gender"] = user.Gender;
                     o["Username"] = user.Username;
                     o["group_id"] = user.group_id;
                    o["Registration_number"] = user.Registration_number;
                    o["Suspended"] = user.Suspended;
                    o["Blocked"] = user.Blocked;
                    o["SuspendedUntil"] = user.SuspendedUntil;
                    try { o["group_name"] = user.Group.Name;
                     }
                     catch (Exception)
                     {
                         o["group_name"] = "";
                     }
                     o["Id"] = user.Id;
                     o["Student"] = user.Student;
                     var response=Request.CreateResponse(HttpStatusCode.Accepted, o);
                     _unitOfWork.LogsRepository.Add(new Logs { Date = DateTime.Now, user_id = userId });
                     _unitOfWork.Save();

                     response.Headers.Add("Token", token.AuthToken);
                     response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);
                     response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");                     
                     return response;
                 }
                 else
                 {
                     var user = _unitOfWork.UserRepository.GetByID(userId);
                     JObject o = new JObject();
                     o["First_name"] = user.First_name;
                     o["Last_name"] = user.Last_name;
                     o["Gender"] = user.Gender;
                     o["Username"] = user.Username;
                     o["group_id"] = user.group_id;
                    o["Registration_number"] = user.Registration_number;
                    o["Suspended"] = user.Suspended;
                    o["Blocked"] = user.Blocked;
                    o["SuspendedUntil"] = user.SuspendedUntil;
                    try
                     {
                         o["group_name"] = user.Group.Name;
                     }
                     catch (Exception)
                     {
                         o["group_name"] = "";
                     }
                     o["Id"] = user.Id;
                     o["Student"] = user.Student;
                     var response = Request.CreateResponse(HttpStatusCode.Accepted, o);
                     _unitOfWork.LogsRepository.Add(new Logs { Date = DateTime.Now, user_id = userId });
                     _unitOfWork.Save();

                     response.Headers.Add("Token", tokenAuth.AuthToken);
                     response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);
                     response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
                     return response;
                 }
             }
             catch (Exception)
             {
                 var token = _tokenServices.GenerateToken(userId);
                 var user = _unitOfWork.UserRepository.GetByID(userId);
                 JObject o = new JObject();
                 o["First_name"] = user.First_name;
                 o["Last_name"] = user.Last_name;
                 o["Gender"] = user.Gender;
                 o["Username"] = user.Username;
                 o["group_id"] = user.group_id;
                o["Registration_number"] = user.Registration_number;
                o["Suspended"] = user.Suspended;
                o["Blocked"] = user.Blocked;
                o["SuspendedUntil"] = user.SuspendedUntil;
                try
                 {
                     o["group_name"] = user.Group.Name;
                 }
                 catch (Exception)
                 {
                     o["group_name"] = "";
                 }
                 o["Id"] = user.Id;
                 o["Student"] = user.Student;
                 var response = Request.CreateResponse(HttpStatusCode.Accepted, o);
                 _unitOfWork.LogsRepository.Add(new Logs { Date = DateTime.Now, user_id = userId });
                 _unitOfWork.Save();

                 response.Headers.Add("Token", token.AuthToken);
                 response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);
                 response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
                 return response;
             }
            
         }
             
        
    }
}
