﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("userHomeworks")]
    public class UserHomeworksController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        [Route("{homeworkId:int}")]
        [HttpGet]
        [AuthorizationRequired]
        public IHttpActionResult GetUserHomework(int homeworkId)
        {
                int userId = (int)ControllerContext.RouteData.Values["userId"];
                UserHomeworks homework = _unitOfWork.UserHomeworkRepository.Get(hw => hw.user_id == userId && hw.homework_id == homeworkId);
                if (homework == null)
                    return NotFound();
                JObject o = new JObject();
                o["Id"] = homework.Id;
                o["Answer"] = homework.Answer;
                o["homework_id"] = homework.homework_id;
                o["user_id"] = homework.user_id;
                o["Username"] = homework.User.Username;
                return Ok(o);            
        }

        // PUT: api/UserHomeworks/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserHomeworks(int id, UserHomeworks userHomeworks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userHomeworks.Id)
            {
                return BadRequest();
            }

            db.Entry(userHomeworks).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserHomeworksExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserHomeworks
        [ResponseType(typeof(JObject))]
        [Route("")]
        [HttpPost]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PostUserHomeworks(UserHomeworks userHomeworks)
        {
            if (db.Homeworks.Find(userHomeworks.homework_id).End_date < DateTime.Now)
                return BadRequest("Perioada de completare a temei a trecut");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            userHomeworks.user_id = userId;
            db.UserHomeworks.Add(userHomeworks);
            await db.SaveChangesAsync();

            JObject o = new JObject();
            o["Id"] = userHomeworks.Id;
            o["Answer"] = userHomeworks.Answer;
            o["homework_id"] = userHomeworks.homework_id;
            o["user_id"] = userHomeworks.user_id;
            return Ok(o);
        }

        // DELETE: api/UserHomeworks/5
        [Route("{id:int}")]
        [HttpDelete]
        [ResponseType(typeof(UserHomeworks))]
        public async Task<IHttpActionResult> DeleteUserHomeworks(int id)
        {
            UserHomeworks userHomeworks = await db.UserHomeworks.FindAsync(id);
            if (userHomeworks == null)
            {
                return NotFound();
            }

            db.UserHomeworks.Remove(userHomeworks);
            await db.SaveChangesAsync();

            return Ok(userHomeworks);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserHomeworksExists(int id)
        {
            return db.UserHomeworks.Count(e => e.Id == id) > 0;
        }
    }
}