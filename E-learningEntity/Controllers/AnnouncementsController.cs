﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using E_learningEntity.Services;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("announcements")]
    public class AnnouncementsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Announcements/5
        [Route("{id:int}")]
        [ResponseType(typeof(Announcement))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> GetAnnouncement(int id)
        {
            Announcement announcement = await db.Announcements.FindAsync(id);
            if (announcement == null)
            {
                return NotFound();
            }

            return Ok(announcement);
        }

        // PUT: api/Announcements/5
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutAnnouncement(int id, Announcement announcement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != announcement.Id)
            {
                return BadRequest();
            }

            db.Entry(announcement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnnouncementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Announcements
        [Route("")]
        [ResponseType(typeof(Announcement))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PostAnnouncement(Announcement announcement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //trimite notificari
            if (announcement.group_id != null)
            {
                Notification notification = new Notification { Message = "Un nout anunt a fost postat pe grupul " + db.Groups.Find(announcement.group_id).Name };
                db.Notifications.Add(notification);

                foreach (User user in _unitOfWork.UserRepository.GetManyQueryable(obj => obj.group_id == announcement.group_id))
                    db.UserNotifications.Add(new UserNotification { Message = notification.Message, notification_id = notification.Id, Seen = false, user_id = (int)user.Id });
            }
            db.Announcements.Add(announcement);
            await db.SaveChangesAsync();

            return Ok(announcement);
        }

        [Route("{courseId:int}/announcement")]
        [ResponseType(typeof(Announcement))]
        [CourseTeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PostAnnouncement(Announcement announcement,int courseId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            announcement.course_id = courseId;
            Course course= await db.Courses.FindAsync(courseId);
            announcement.Course = course;

            if (announcement.course_id != null)
            {
                Notification notification = new Notification { Message = "Un nout anunt a fost postat pe cursul " + course.Name };
                db.Notifications.Add(notification);

                foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(obj => obj.course_id == courseId && obj.Role.Equals("Student")))
                    db.UserNotifications.Add(new UserNotification { Message = notification.Message, notification_id = notification.Id, Seen = false, user_id = (int)perm.user_id });
            }

            db.Announcements.Add(announcement);
            await db.SaveChangesAsync();
            return Ok(announcement);
        }

        // DELETE: api/Announcements/5
        [Route("{id}")]
        [ResponseType(typeof(Announcement))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteAnnouncement(int id)
        {
            Announcement announcement = await db.Announcements.FindAsync(id);
            if (announcement == null)
            {
                return NotFound();
            }

            db.Announcements.Remove(announcement);
            await db.SaveChangesAsync();

            return Ok(announcement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnnouncementExists(int id)
        {
            return db.Announcements.Count(e => e.Id == id) > 0;
        }
    }
}