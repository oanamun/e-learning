﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using E_learningEntity.Services;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("events")]
    public class EventsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();


        // GET: api/Events/5
        [Route("{eventId:int}")]
        [ResponseType(typeof(Event))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> GetEvent(int eventId)
        {
            Event @event = await db.Events.FindAsync(eventId);
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (userId != @event.user_id)
                return StatusCode(HttpStatusCode.Forbidden);
            if (@event == null)
                return NotFound();
            return Ok(@event);
        }

        // PUT: api/Events/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PutEvent(int id, Event @event)
        {
           // int userId = (int)ControllerContext.RouteData.Values["userId"];
          //  if (userId != @event.user_id)
          //      return Unauthorized();

                if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != @event.Id)
            {
                return BadRequest();
            }

            db.Entry(@event).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Events
        [ResponseType(typeof(Event))]
        [AuthorizationRequired]
        [Route("")]
        [HttpPost]
        public IHttpActionResult PostEvent(Event @event)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            @event.user_id = userId;
            _unitOfWork.EventRepository.Add(@event);
            _unitOfWork.Save();

            return Ok(@event);
        }

        // DELETE: api/Events/5
        [ResponseType(typeof(Event))]
        [Route("")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteEvent(int id)
        {
            Event @event = await db.Events.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            db.Events.Remove(@event);
            await db.SaveChangesAsync();

            return Ok(@event);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventExists(int id)
        {
            return db.Events.Count(e => e.Id == id) > 0;
        }


        [AuthorizationRequired]
        [Route("myevents")]
        [HttpGet]
        public List<JObject> GetMyEvents()
        {
            List<JObject> schoolEvents = new List<JObject>();
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            IQueryable<Permission> myPermissions = _unitOfWork.PermissionRepository.GetManyQueryable(permission => permission.user_id == userId);
            foreach (Permission perm in myPermissions)
            {
                IQueryable<Module> myModules = _unitOfWork.ModuleRepository.GetManyQueryable(module=>module.course_id ==perm.course_id);
                foreach (Module module in myModules)
                {
                    IQueryable<Test> myTests = _unitOfWork.TestRepository.GetManyQueryable(test => test.module_id == module.Id);
                    foreach (Test test in myTests)
                        if (test.Due_date > DateTime.Now)
                        {
                            JObject o = new JObject();
                            o["Name"] = test.Name;
                            o["Description"] = test.Description;
                            o["End_date"] = test.Due_date;
                            schoolEvents.Add(o);
                        }
                    IQueryable<Homework> myHomeworks = _unitOfWork.HomeworkRepository.GetManyQueryable(test => test.module_id == module.Id);
                    foreach (Homework homework in myHomeworks)
                        if (homework.End_date > DateTime.Now)
                        {
                            JObject o = new JObject();
                            o["Name"] = homework.Name;
                            o["Description"] = homework.Description;
                            o["End_date"] = homework.End_date;
                            schoolEvents.Add(o);
                        }
                }
            }
            foreach (Event ev in _unitOfWork.EventRepository.GetManyQueryable(ev => ev.user_id == userId))
            {
                JObject o = new JObject();
                o["Name"] = ev.Name;
                o["Description"] = ev.Description;
                o["End_date"] = ev.End_date;
                schoolEvents.Add(o);
            }
            return schoolEvents;
        }    
    }

   
}