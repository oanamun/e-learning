﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("modules")]
    public class ModulesController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Modules/5
        [Route("{moduleId:int}")]
        [HttpGet]
        [ResponseType(typeof(JObject))]
        [CoursePermissionRequired]
        public async Task<IHttpActionResult> GetModule(int moduleId)
        {
            Module module = await db.Modules.FindAsync(moduleId);
            if (module == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = module.Id;
            o["Name"] = module.Name;
            o["Start_date"] = module.Start_date;
            o["Content"] = module.Content;
            o["Description"] = module.Description;
            o["course_id"] = module.course_id;
            return Ok(o);
        }

        // PUT: api/Modules/5
        [Route("{moduleId:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PutModule(int moduleId, Module module)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == module.course_id && perm.Role.Equals("Profesor")) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (moduleId != module.Id)
            {
                return BadRequest();
            }

            db.Entry(module).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModuleExists(moduleId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [Route("")]
        [HttpPost]
        [ResponseType(typeof(JObject))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PostModule(Module module)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == module.course_id && perm.Role.Equals("Profesor")) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            if (_unitOfWork.ModuleRepository.Get(obj => obj.Name.Equals(module.Name) && obj.Description.Equals(module.Description) && obj.course_id==module.course_id) != null)
                return BadRequest("Modul duplicat");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //trimite notificare          
            Notification notification = new Notification { Message = "De astazi un nou modul este valabil:  " + module.Name+" din cursul "+ db.Courses.Find(module.course_id).Name, course_id = module.course_id };
            db.Notifications.Add(notification);
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(obj => obj.course_id == module.course_id && obj.Role.Equals("Student")))
                db.UserNotifications.Add(new UserNotification { Message = notification.Message, notification_id = notification.Id, Seen = false, user_id = (int)perm.user_id });

            db.Modules.Add(module);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["Id"] = module.Id;
            o["Name"] = module.Name;
            o["Start_date"] = module.Start_date;
            o["Content"] = module.Content;
            o["Description"] = module.Description;
            o["course_id"] = module.course_id;
            return Ok(o);
        }

        // DELETE: api/Modules/5
        [Route("{moduleId:int}")]
        [HttpDelete]
        [ResponseType(typeof(Module))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> DeleteModule(int moduleId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Module module = db.Modules.Find(moduleId);
            if (module == null)
                return NotFound();
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == module.course_id && perm.Role.Equals("Profesor")) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            db.Modules.Remove(module);
            await db.SaveChangesAsync();

            return Ok(module);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ModuleExists(int id)
        {
            return db.Modules.Count(e => e.Id == id) > 0;
        }


        //get all homeworks for a module
        [Route("{moduleId:int}/homeworks")]
        [HttpGet]
        [CoursePermissionRequired]
        public ICollection<JObject> GetHomeworksForModuleID(int moduleId)
        {
            List<JObject> list = new List<JObject>();
            foreach (Homework h in _unitOfWork.HomeworkRepository.GetManyQueryable(homework => homework.module_id == moduleId))
            {
                JObject o = new JObject();
                o["Id"] = h.Id;
                o["Name"] = h.Name;
                o["Description"] = h.Description;
                o["Format"] = h.Format;
                o["End_date"] = h.End_date;
                o["Grading_type"] = h.Grading_type;
                o["Required"] = h.Required;
                o["Pondere"] = h.Pondere;
                o["module_id"] = h.module_id;
                list.Add(o);
            }
            return list;
        }

        //get all tests for a module
        [Route("{moduleId:int}/tests")]
        [HttpGet]
        [CoursePermissionRequired]
        public ICollection<JObject> GetTestsForModuleID(int moduleId)
        {
            List<JObject> list = new List<JObject>();
            foreach (Test test in _unitOfWork.TestRepository.GetManyQueryable(test => test.module_id == moduleId))
            {
                JObject o = new JObject();
                o["Id"] = test.Id;
                o["Name"] = test.Name;
                o["Description"] = test.Description;
                o["Due_date"] = test.Due_date;
                o["Type"] = test.Type;
                o["Pondere"] = test.Pondere;
                o["module_id"] = test.module_id;
                list.Add(o);
            }
            return list;
        }
    }
}