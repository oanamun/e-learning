﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
   public class PagesController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();


        // GET: api/Pages/5
        [ResponseType(typeof(JObject))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> GetPage(int id)
        {
            Page page = await db.Pages.FindAsync(id);
            if (page == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = page.Id;
            o["Text"] = page.Text;
            o["Title"] = page.Title;
            o["group_id"] = page.group_id;
            o["user_id"] = page.user_id;
            o["Username"] = page.User.Username;
            return Ok(o);
        }

        // PUT: api/Pages/5
        [ResponseType(typeof(void))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PutPage(int id, Page page)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != page.Id)
            {
                return BadRequest();
            }

            db.Entry(page).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pages
        [ResponseType(typeof(JObject))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PostPage(Page page)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            page.user_id = userId;
            db.Pages.Add(page);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["Id"] = page.Id;
            o["Text"] = page.Text;
            o["Title"] = page.Title;
            o["group_id"] = page.group_id;
            o["user_id"] = page.user_id;
            o["Username"] = page.User.Username;
            return Ok(o);
        }

        // DELETE: api/Pages/5
        [ResponseType(typeof(Page))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> DeletePage(int id)
        {
            Page page = await db.Pages.FindAsync(id);
            if (page == null)
            {
                return NotFound();
            }

            db.Pages.Remove(page);
            await db.SaveChangesAsync();

            return Ok(page);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PageExists(int id)
        {
            return db.Pages.Count(e => e.Id == id) > 0;
        }
    }
}