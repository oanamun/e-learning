﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("forum")]
    public class DiscussionsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        private UnitOfWork _unitOfWork = new UnitOfWork();

        [Route("")]
        [AuthorizationRequired]
        public ICollection<JObject> GetForum()
        {
            List<JObject> list = new List<JObject>();
            foreach(Discussion discussion in _unitOfWork.DiscussionRepository.GetAll())
            {
                if (discussion.course_id== null)
                {
                    JObject o = new JObject();
                    o["Id"] = discussion.Id;
                    o["Subject"] = discussion.Subject;
                    o["course_id"] = discussion.course_id;
                    o["Description"] = discussion.Description;
                    list.Add(o);
                }
            }
            return list;
        }

        // GET: /forum/discId
        [Route("{id:int}")]
        [ResponseType(typeof(JObject))]
        public async Task<IHttpActionResult> GetDiscussion(int id)
        {
            Discussion discussion = await db.Discussions.FindAsync(id);
            if (discussion == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = discussion.Id;
            o["Subject"] = discussion.Subject;
            o["Description"] = discussion.Description;
            o["Category"] = discussion.Category;
            if (discussion.course_id != null)
            {
                o["course_id"] = discussion.course_id;
                o["Course"] = discussion.Course.Name;
            } else 
            {
                o["course_id"] = discussion.course_id;
            }
            return Ok(o);
        }

        // PUT: api/Discussions/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDiscussion(int id, Discussion discussion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != discussion.Id)
            {
                return BadRequest();
            }

            db.Entry(discussion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiscussionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [Route("")]
        [HttpPost]
        [ResponseType(typeof(Discussion))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PostDiscussion(Discussion discussion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Discussions.Add(discussion);
            await db.SaveChangesAsync();

            return Ok(discussion);
        }

        // DELETE: api/Discussions/5
        [Route("{id}")]
        [ResponseType(typeof(Discussion))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> DeleteDiscussion(int id)
        {
            Discussion discussion = await db.Discussions.FindAsync(id);
            if (discussion == null)
            {
                return NotFound();
            }
            foreach (Comment comment in db.Comments.Where(obj => obj.discussion_id == id).ToList())
            {
                db.Entry(comment).State = EntityState.Deleted;
                db.SaveChanges();
            }

            foreach(Grade grade in db.Grades.Where(obj => obj.discussion_id == id).ToList())
            {
                db.Entry(grade).State = EntityState.Deleted;
                db.SaveChanges();
            }

            //db.Discussions.Remove(discussion);
            db.Entry(discussion).State = EntityState.Deleted;
            await db.SaveChangesAsync();
            return Ok(discussion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DiscussionExists(int id)
        {
            return db.Discussions.Count(e => e.Id == id) > 0;
        }


        //get all comments for a discussion /forum/discussionId/comments
        [Route("{discussionId:int}/comments")]
        [HttpGet]
        [AuthorizationRequired]
        public ICollection<JObject> GetCommentsForDiscussionID(int discussionId)
        {
            IQueryable<Comment> comments = _unitOfWork.CommentRepository.GetManyQueryable(comment => comment.discussion_id == discussionId);
            List<JObject> list = new List<JObject>();
            foreach (Comment com in comments)
            {
                JObject o = new JObject();
                o["Id"] = com.Id;
                o["Message"] = com.Message;
                o["Date"] = com.Date;
                o["discussion_id"] = com.discussion_id;
                o["user_name"] = com.User.Username;
                try
                {
                    o["group_name"] = com.User.Group.Name; ;
                }
                catch (Exception)
                {
                    o["group_name"] = "";
                }
                list.Add(o);
            }
            return list;
        }


        //get all grades for a discussion /forum/discId/grades
        [Route("{discussionId:int}/grades")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<Grade> GetGradesForDiscussionID(int discussionId)
        {
            return _unitOfWork.GradeRepository.GetManyQueryable(grade => grade.discussion_id == discussionId);
        }


        [Route("{discussionId:int}/participants")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetParticipantsToDiscussion(int discussionId)
        {
            if (db.Discussions.Find(discussionId) == null)
                return NotFound();
            IQueryable<Comment> comments = _unitOfWork.CommentRepository.GetManyQueryable(comment => comment.discussion_id == discussionId);
            List<JObject> list = new List<JObject>();
            List<int> usersId = new List<int>();
            foreach (Comment com in comments)
            {
                if (!usersId.Contains(com.user_id))
                {
                    usersId.Add(com.user_id);
                    JObject o = new JObject();
                    o["Id"] = com.user_id;
                    o["Username"] = com.User.Username;
                    list.Add(o);
                }
            }
            return Ok(list);
        }
   }
}