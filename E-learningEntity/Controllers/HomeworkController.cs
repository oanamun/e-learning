﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("homeworks")]
    public class HomeworkController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        private UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Homework
        [Route("myhomeworks")]
        [AuthorizationRequired]
        public List<JObject> GetHomeworks()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            List<int> modulesId = new List<int>();
            List<int> myCourses = new List<int>();
            List<Homework> myHomeworks = new List<Homework>();
            List<Module> myModules = new List<Module>();
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId))
                if (perm.course_id != null)
                    if (!myCourses.Contains((int)perm.course_id))
                        myCourses.Add((int)perm.course_id);
            foreach (int courseId in myCourses)
                myModules.AddRange(_unitOfWork.ModuleRepository.GetManyQueryable(module => module.course_id == courseId));
            foreach (Module module in myModules)
                modulesId.Add(module.Id);
            foreach (int id in modulesId)
                myHomeworks.AddRange(_unitOfWork.HomeworkRepository.GetManyQueryable(homework => homework.module_id == id));
            List<JObject> list = new List<JObject>();
            foreach (Homework h in myHomeworks)
            {
                JObject o = new JObject();
                o["Id"] = h.Id;
                o["Name"] = h.Name;
                o["Description"] = h.Description;
                o["Format"] = h.Format;
                o["End_date"] = h.End_date;
                o["Grading_type"] = h.Grading_type;
                o["Required"] = h.Required;
                o["Pondere"] = h.Pondere;
                o["module_id"] = h.module_id;
                o["Module"] = h.Module.Name;
                o["Course"] = h.Module.Course.Name;
                o["group_id"] = h.group_id;
                list.Add(o);
           }
            return list;
        }


        // GET: api/Homework/5
        [Route("{id:int}")]
        [HttpGet]
        [ResponseType(typeof(JObject))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> GetHomework(int id)
        {
            Homework h = await db.Homeworks.FindAsync(id);
            if (h == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = h.Id;
            o["Name"] = h.Name;
            o["Description"] = h.Description;
            o["Format"] = h.Format;
            o["End_date"] = h.End_date;
            o["Grading_type"] = h.Grading_type;
            o["Required"] = h.Required;
            o["Pondere"] = h.Pondere;
            o["module_id"] = h.module_id;
            o["Module"] = h.Module.Name;
            o["Course"] = h.Module.Course.Name;
            o["group_id"] = h.group_id;
            return Ok(o);
        }

        // PUT: api/Homework/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PutHomework(int id, Homework homework)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != homework.Id)
            {
                return BadRequest();
            }

            db.Entry(homework).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HomeworkExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Homework
        //TODO Auth
        [Route("")]
        [HttpPost]
        [ResponseType(typeof(Homework))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PostHomework(Homework h)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (_unitOfWork.HomeworkRepository.Get(obj => obj.Description.Equals(h.Description) && obj.Name == h.Name) != null)
                return BadRequest("Tema duplicata");

            db.Homeworks.Add(h);
            await db.SaveChangesAsync();
            Module module = _unitOfWork.ModuleRepository.GetByID(h.module_id);

            Notification notification = new Notification { Message = "De astazi puteti incepe sa rezolvati o noua tema: " + h.Name + " valabil la modulul" + _unitOfWork.ModuleRepository.GetByID(h.module_id).Name, course_id = module.course_id };
            db.Notifications.Add(notification);
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(obj => obj.course_id == module.course_id && obj.Role.Equals("Student")))
                db.UserNotifications.Add(new UserNotification { Message = notification.Message, notification_id = notification.Id, Seen = false, user_id = (int)perm.user_id });
            await db.SaveChangesAsync();

            JObject o = new JObject();
            o["Id"] = h.Id;
            o["Name"] = h.Name;
            o["Description"] = h.Description;
            o["Format"] = h.Format;
            o["End_date"] = h.End_date;
            o["Grading_type"] = h.Grading_type;
            o["Required"] = h.Required;
            o["Pondere"] = h.Pondere;
            o["module_id"] = h.module_id;
            o["group_id"] = h.group_id;
            return Ok(o);
        }

        // DELETE: api/Homework/5
        [ResponseType(typeof(Homework))]
        [TeacherAuthorizationRequired]
        [Route("{id}")]
        public async Task<IHttpActionResult> DeleteHomework(int id)
        {
            Homework homework = await db.Homeworks.FindAsync(id);
            if (homework == null)
            {
                return NotFound();
            }

            foreach (Grade grade in db.Grades.Where(obj => obj.homework_id == id).ToList())
            {
                db.Entry(grade).State = EntityState.Deleted;
                db.SaveChanges();
            }

            db.Entry(homework).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Ok(homework);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HomeworkExists(int id)
        {
            return db.Homeworks.Count(e => e.Id == id) > 0;
        }

        //ia rezolvarile pentru o tema sa le vada profu
        [Route("{homeworkId:int}/submitted")]
        [HttpGet]
        [TeacherAuthorizationRequired]
        public ICollection<JObject> GetUserHomeworks(int homeworkId)
        {
           IQueryable<UserHomeworks> userHomeworks= _unitOfWork.UserHomeworkRepository.GetManyQueryable(hw => hw.homework_id == homeworkId);
           List<JObject> list = new List<JObject>();
           foreach (UserHomeworks userH in userHomeworks)
           {
               Grade gradeH = _unitOfWork.GradeRepository.Get(grade => grade.homework_id == homeworkId && grade.user_id == userH.user_id);
               JObject o = new JObject();
               o["Id"] = userH.Id;
               o["Answer"] = userH.Answer;
               o["homework_id"] = userH.homework_id;
               o["user_id"] = userH.user_id;
               o["Username"] = userH.User.Username;
               o["Module"] = userH.Homework.Module.Name;
                if (gradeH != null)
                    o["Grade"] = gradeH.Value;
                else o["Grade"] = null;
               list.Add(o);
           }
           return list;
               
        }
    }
}