﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("tests")]
    public class TestsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();        

        [Route("mytests")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetMyTests()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            List<int> modulesId = new List<int>();
            List<int> myCourses = new List<int>();
            List<Test> myTests = new List<Test>();
            List<Module> myModules = new List<Module>();
            List<JObject> list = new List<JObject>();
            IQueryable<Permission> myPermissions = _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId);     
            foreach (Permission perm in myPermissions)
                if(perm.course_id!=null)
                    if(!myCourses.Contains((int)perm.course_id))
                        myCourses.Add((int)perm.course_id);    
            foreach (int courseId in myCourses)
                myModules.AddRange(_unitOfWork.ModuleRepository.GetManyQueryable(module => module.course_id == courseId));            
            foreach (Module module in myModules)
                modulesId.Add(module.Id);
            foreach (int id in modulesId)
                myTests.AddRange(_unitOfWork.TestRepository.GetManyQueryable(test => test.module_id == id));
            foreach (Test test in myTests)
            {
                JObject o = new JObject();
                o["Id"] = test.Id;
                o["Name"] = test.Name;
                o["Description"] = test.Description;
                o["Due_date"] = test.Due_date;
                o["Type"] = test.Type;
                o["Pondere"] = test.Pondere;
                o["module_id"] = test.module_id;
                o["Module"] = test.Module.Name;
                o["Course"] = test.Module.Course.Name;
                list.Add(o);
            }
            return list;
        }

        // GET: api/Tests/5
        [Route("{id:int}")]
        [ResponseType(typeof(JObject))]
        [AuthorizationRequired]
        public IHttpActionResult GetTest(int id)
        {
            Test test = db.Tests.Find(id);
            if (test == null)
                return NotFound();
            JObject o = new JObject();
            o["Id"] = test.Id;
            o["Name"] = test.Name;
            o["Description"] = test.Description;
            o["Due_date"] = test.Due_date;
            o["Type"] = test.Type;
            o["Pondere"] = test.Pondere;
            o["module_id"] = test.module_id;
            o["Module"] = test.Module.Name;
            o["Course"] = test.Module.Course.Name;
            return Ok(o);
        }

        // PUT: api/Tests/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PutTest(int id, Test test)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == _unitOfWork.ModuleRepository.GetByID(test.module_id).course_id && perm.Role.Equals("Profesor")) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != test.Id)
            {
                return BadRequest();
            }

            db.Entry(test).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("")]
        [HttpPost]
        [ResponseType(typeof(JObject))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PostTest(Test test)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Module module = _unitOfWork.ModuleRepository.GetByID(test.module_id);
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == module.course_id && perm.Role.Equals("Profesor")) == null)
                return StatusCode(HttpStatusCode.Forbidden);


            if (_unitOfWork.TestRepository.Get(obj => obj.Name.Equals(test.Name) && obj.Description.Equals(test.Description) && obj.module_id==test.module_id) != null)
                return BadRequest("Test duplicat");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tests.Add(test);
            Notification notification = new Notification { Message = "De astazi puteti incepe sa rezolvati un nou test:  " + test.Name+" valabil la modulul" + _unitOfWork.ModuleRepository.GetByID(test.module_id).Name, course_id = module.course_id };
            db.Notifications.Add(notification);
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(obj => obj.course_id == module.course_id && obj.Role.Equals("Student")))
                db.UserNotifications.Add(new UserNotification { Message = notification.Message, notification_id = notification.Id, Seen = false, user_id = (int)perm.user_id});
            await db.SaveChangesAsync();

            JObject o = new JObject();
            o["Id"] = test.Id;
            o["Name"] = test.Name;
            o["Description"] = test.Description;
            o["Due_date"] = test.Due_date;
            o["Type"] = test.Type;
            o["Pondere"] = test.Pondere;
            o["module_id"] = test.module_id;
            o["Module"] = _unitOfWork.ModuleRepository.GetByID(test.module_id).Name;
            return Ok(o);
        }

        // DELETE: api/Tests/5
        [Route("{id:int}")]
        [HttpDelete]
        [ResponseType(typeof(Test))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> DeleteTest(int id)
        {
            Test test = await db.Tests.FindAsync(id);
            if (test == null)
            {
                return NotFound();
            }

            foreach (Grade grade in db.Grades.Where(obj => obj.test_id == id).ToList())
            {
                db.Entry(grade).State = EntityState.Deleted;
                db.SaveChanges();
            }

            db.Entry(test).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Ok(test);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TestExists(int id)
        {
            return db.Tests.Count(e => e.Id == id) > 0;
        }


        [Route("{testId:int}/questions")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetQuestionForTestID(int testId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Test test = _unitOfWork.TestRepository.GetByID(testId);
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == test.Module.course_id) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            if (test.Due_date < DateTime.Now && db.Users.Find(userId).Student==true)
                return BadRequest("Deadline-ul a trecut");

            List<JObject> list = new List<JObject>();
            foreach (Question question in _unitOfWork.QuestionRepository.GetManyQueryable(question => question.test_id == testId))
            {
                JObject o = new JObject();
                o["Id"] = question.Id;
                o["Description"] = question.Description;
                o["Pondere"] = question.Pondere;
                o["test_id"] = question.test_id;
                list.Add(o);
            }
            return Ok(list);
        }

        [Route("{testId:int}/count")]
        [HttpGet]
        public IHttpActionResult GetNrOfQuestionsForTest(int testId)
        {
            return Ok(_unitOfWork.QuestionRepository.GetManyQueryable(obj => obj.test_id == testId).ToList().Count);
        }
        
        //get grades for a test
        [Route("{testId:int}/grades")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetGradesForTestID(int testId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Test test = _unitOfWork.TestRepository.GetByID(testId);
            if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == userId && perm.course_id == test.Module.course_id && perm.Role.Equals("Profesor")) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            List<JObject> list = new List<JObject>();
            foreach (Grade grade in _unitOfWork.GradeRepository.GetManyQueryable(grade => grade.test_id == testId))
            {
                JObject o = new JObject();
                o["Value"] = grade.Value;
                o["Name"] = grade.Test.Name;
                o["test_id"] = grade.test_id;
                o["Username"] = grade.User.Username;
                list.Add(o);                
            }
            return Ok(list);
        }
    }
}