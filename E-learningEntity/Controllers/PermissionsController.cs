﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using E_learningEntity.Services;
using System;

namespace E_learningEntity.Controllers
{
   // [RoutePrefix("permissions")]
    public class PermissionsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Permissions
        public IQueryable<Permission> GetPermissions()
        {
            return db.Permissions;
        }

        // GET: api/Permissions/5
        [ResponseType(typeof(Permission))]
        //[Route("{id:int}")]
        public async Task<IHttpActionResult> GetPermission(int id)
        {
            Permission permission = await db.Permissions.FindAsync(id);
            if (permission == null)
            {
                return NotFound();
            }

            return Ok(permission);
        }

        // PUT: api/Permissions/5
        [ResponseType(typeof(void))]
       // [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutPermission(int id, Permission permission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != permission.Id)
            {
                return BadRequest();
            }

            db.Entry(permission).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PermissionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Permissions
        [ResponseType(typeof(Permission))]
        [AuthorizationRequired]
        [HttpPost]
        public async Task<IHttpActionResult> PostPermission(Permission permission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Course course = _unitOfWork.CourseRepository.GetByID(permission.course_id);
            //if (course.Start_date < DateTime.Today && course.End_date > DateTime.Today)
            //{
                //User user = db.Users.Find(permission.user_id);
                //if(user.Student==false)
                //    return BadRequest();
                db.Permissions.Add(permission);
                await db.SaveChangesAsync();
                return Ok(permission);
            //}
            //else return BadRequest("You cannot join this course");
        }

        //[Route("admin")]
        //[ResponseType(typeof(Permission))]
        //[AdminAuthorizationRequired]
        //[HttpPost]
        //public async Task<IHttpActionResult> PostPermissionAdmin(JObject permission)
        //{
        //    User user1= _unitOfWork.UserRepository.Get(user=>user.Username.Equals(permission["Username"]));
        //    Permission perm = new Permission { user_id = user1.Id, Role = (string)permission["Role"], course_id =(int)permission["course_id"] };
        //    if (user1.Student == false)
        //        return BadRequest();
        //    db.Permissions.Add(perm);
        //    await db.SaveChangesAsync();
        //    return Ok(permission);
        //}


        // DELETE: api/Permissions/5
        [ResponseType(typeof(Permission))]
        public async Task<IHttpActionResult> DeletePermission(int id)
        {
            Permission permission = await db.Permissions.FindAsync(id);
            if (permission == null)
            {
                return NotFound();
            }

            db.Permissions.Remove(permission);
            await db.SaveChangesAsync();

            return Ok(permission);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PermissionExists(int id)
        {
            return db.Permissions.Count(e => e.Id == id) > 0;
        }
    }
}