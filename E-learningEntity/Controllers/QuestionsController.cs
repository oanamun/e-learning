﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using Newtonsoft.Json.Linq;
using E_learningEntity.ActionFilters;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("questions")]
    [AuthorizationRequired]
    public class QuestionsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        private UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Questions/5
        [ResponseType(typeof(JObject))]
        [Route("{id:int}")]
        [HttpGet]      
        public async Task<IHttpActionResult> GetQuestion(int id)
        {
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = question.Id;
            o["Description"] = question.Description;
            o["Pondere"] = question.Pondere;
            o["test_id"] = question.test_id;
            return Ok(o);
        }

        // PUT: api/Questions/5
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutQuestion(int id, Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.Id)
            {
                return BadRequest();
            }

            db.Entry(question).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Questions
        [ResponseType(typeof(JObject))]
        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> PostQuestion(Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Questions.Add(question);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["Id"] = question.Id;
            o["Description"] = question.Description;
            o["Pondere"] = question.Pondere;
            o["test_id"] = question.test_id;
            return Ok(o);
        }

        // DELETE: api/Questions/5
        [ResponseType(typeof(Question))]
        [Route("{id:int}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteQuestion(int id)
        {
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }

            db.Questions.Remove(question);
            await db.SaveChangesAsync();

            return Ok(question);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QuestionExists(int id)
        {
            return db.Questions.Count(e => e.Id == id) > 0;
        }


        [Route("{questionId:int}/answers")]
        [AuthorizationRequired]
        [HttpGet]
        public ICollection<JObject> GetAnswerOptionsForQuestion(int questionId)
        {
            List<JObject> list = new List<JObject>();
            User user = _unitOfWork.UserRepository.GetByID((int)ControllerContext.RouteData.Values["userId"]);
            foreach (AnswerOptions option in _unitOfWork.AnswerOptionsRepository.GetManyQueryable(answer => answer.question_id == questionId))
            {
                JObject o = new JObject();
                o["Id"] = option.Id;
                o["Description"] = option.Description;
                o["question_id"] = option.question_id;
                if (user.Student == false)
                    o["Correct"] = option.Correct;
                list.Add(o);
            }
            return list;
        }
    }
}