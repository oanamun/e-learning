﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Filters;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("users")]
    public class UsersController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        /*
             * GetUsers
             *
             * Get all users
             *
             * @return (ICollection<JObject>)
             */
        [Route("details")]
        [HttpGet]
        [AdminAuthorizationRequired]
        public ICollection<JObject> GetUsers()
        {
            List<JObject> list = new List<JObject>();
            foreach (User user in _unitOfWork.UserRepository.GetAll())
            {
                JObject o = new JObject();
                o["First_name"] = user.First_name;
                o["Last_name"] = user.Last_name;
                o["Gender"] = user.Gender;
                o["Username"] = user.Username;
                o["Email"] = user.Email;
                o["Registration_number"] = user.Registration_number;
                o["Suspended"] = user.Suspended;
                o["Blocked"] = user.Blocked;
                o["SuspendedUntil"] = user.SuspendedUntil;
                o["group_id"] = user.group_id;
                try
                {
                    o["group_name"] = user.Group.Name;
                }
                catch (Exception)
                {
                    o["group_name"] = "";
                }
                o["Id"] = user.Id;
                o["Student"] = user.Student;
                list.Add(o);
            }
            return list;
        }

        /*
             * GetUsersForConversation
             *
             * Get all users for current user conversation
             *
             * @return (ICollection<JObject>)
             */
        [Route("")]
        [HttpGet]
        [AuthorizationRequired]
        public ICollection<JObject> GetUsersForConversation()
        {
            List<JObject> list = new List<JObject>();
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            foreach (User user in _unitOfWork.UserRepository.GetAll())
            {
                if (_unitOfWork.PermissionRepository.Get(perm => perm.user_id == user.Id && perm.Role.Equals("Admin")) == null)
                    if (user.Id != userId)
                        {
                    JObject o = new JObject();
                    o["First_name"] = user.First_name;
                    o["Last_name"] = user.Last_name;
                    o["Gender"] = user.Gender;
                    o["Username"] = user.Username;
                    o["Id"] = user.Id;
                    list.Add(o);
                     }
            }
            return list;
        }

        /*
            * CurrentUser
            *
            * Get the current user based on token
            *
            * @return (JObject)
            */
        [ResponseType(typeof(JObject))]
        [HttpGet]
        [Route("user")]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> CurrentUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = await db.Users.FindAsync(userId);
            if (user == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["First_name"] = user.First_name;
            o["Last_name"] = user.Last_name;
            o["Gender"] = user.Gender;
            o["Username"] = user.Username;
            o["Email"] = user.Email;
            o["Registration_number"] = user.Registration_number;
            o["Suspended"] = user.Suspended;
            o["Blocked"] = user.Blocked;
            o["SuspendedUntil"] = user.SuspendedUntil;
            o["group_id"] = user.group_id;
            try
            {
                o["group_name"] = user.Group.Name;
            }
            catch (Exception)
            {
                o["group_name"] = "";
            }
            o["Id"] = user.Id;
            o["Student"] = user.Student;
            return Ok(o);
        }

        /*
                  * GetUser
                  *
                  * Returns an user by Id
                  *
                  * @userId (int) user id
                  * @return (JObject)
                  */
        [Route("{userId:int}", Name = "GetUserById")]
        [ResponseType(typeof(JObject))]
        [HttpGet]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> GetUser(int userId)
        {
            User user = await db.Users.FindAsync(userId);
            if (user == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["First_name"] = user.First_name;
            o["Last_name"] = user.Last_name;
            o["Gender"] = user.Gender;
            o["Username"] = user.Username;
            o["Email"] = user.Email;
            o["Registration_number"] = user.Registration_number;
            try
            {
                o["group_name"] = user.Group.Name;
            }
            catch (Exception)
            {
                o["group_name"] = "";
            }
            return Ok(o);
        }

        /*
            * PutUser
            *
            * Update user with id
            *
            * @id (int) user id
            * @user (User) user object modified
            * @return (StatusCode)
            */
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //// POST: api/Users
        //[ResponseType(typeof(User))]
        //[Route("")]
        //[HttpPost]
        //[AdminAuthorizationRequired]
        //public async Task<IHttpActionResult> PostUser(User user)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Users.Add(user);
        //    await db.SaveChangesAsync();
        //    return Ok(user);
        //}

        /*
        * PostUser
        *
        * Add new user into db
        *
        * @user (User) user object
        * @return (JObject)
        */
        [ResponseType(typeof(JObject))]
        [Route("")]
        [HttpPost]
        [AdminAuthorizationRequired]
        public async Task<IHttpActionResult> PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string Salt = GetRandomSalt();
            user.Salt = Salt;
            user.Password = GetFinal(user.Password, Salt);

            db.Users.Add(user);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["First_name"] = user.First_name;
            o["Last_name"] = user.Last_name;
            o["Gender"] = user.Gender;
            o["Username"] = user.Username;
            o["Email"] = user.Email;
            o["Registration_number"] = user.Registration_number;
            o["Suspended"] = user.Suspended;
            o["Blocked"] = user.Blocked;
            o["SuspendedUntil"] = user.SuspendedUntil;
            o["group_id"] = user.group_id;
            try
            {
                o["group_name"] = user.Group.Name;
            }
            catch (Exception)
            {
                o["group_name"] = "";
            }
            o["Id"] = user.Id;
            o["Student"] = user.Student;
            return Ok(o);
        }

        
        [ResponseType(typeof(User))]
        [Route("{id:int}")]
        [HttpDelete]
        [AdminAuthorizationRequired]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /*
            * UserExists
            *
            * Verify if a user exists into database
            *
            * @id (int) user id
            *
            * @return (bool)
            */
        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }


        [Route("user/courses/discussions")]
        [HttpGet]
        [AuthorizationRequired]
        public ICollection<JObject> GetUsersDiscussions()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];

            List<int> courseId = new List<int>();
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId && perm.course_id != null))
            {
                if (!courseId.Contains(perm.Course.Id))
                    courseId.Add(perm.Course.Id);
            }

            List<Discussion> UserDiscussions = new List<Discussion>();
            foreach (int id in courseId)
                UserDiscussions.AddRange(_unitOfWork.DiscussionRepository.GetManyQueryable(discussion => discussion.course_id == id));

            List<JObject> list = new List<JObject>();
            foreach (Discussion disc in UserDiscussions)
            {
                JObject o = new JObject();
                o["Id"] = disc.Id;
                o["Subject"] = disc.Subject;
                o["Description"] = disc.Description;
                o["Category"] = disc.Category;
                list.Add(o);
            }
            return list;
        }

        /*
            * GetPermissionsForUser
            *
            * Get current user permissions based on token
            *
            * @return (List<JObject>)
            */
        [Route("user/permissions")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetPermissionsForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            List<JObject> permissions = new List<JObject>();
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(permission => permission.user_id == userId))
            {
                JObject o = new JObject();
                o["Role"] = perm.Role;
                o["user_id"] = perm.user_id;
                permissions.Add(o);
            }
            return permissions;
        }

        /*
             * GetEventsFor
             *
             * Get events for current user
             *
             * @return (ICollection<JObject>)
             */
        [Route("user/events")]
        [HttpGet]
        [ResponseType(typeof(List<Event>))]
        [AuthorizationRequired]
        public ICollection<JObject> GetEventsForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            List<JObject> list = new List<JObject>();
            foreach (Event ev in _unitOfWork.EventRepository.GetManyQueryable(even => even.user_id == userId))
            {
                JObject o = new JObject();
                o["Name"] = ev.Name;
                o["Description"] = ev.Description;
                o["End_date"] = ev.End_date;
                list.Add(o);
            }
            return list;
        }

        /*
            * GetCommentForUser
            *
            * Get comments for curent user
            *
            * @return (IQueryable<Comment>)
            */
        [Route("user/comments")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<Comment> GetCommentsForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            return _unitOfWork.CommentRepository.GetManyQueryable(comment => comment.user_id == userId);

        }

        [Route("user/pages")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<Page> GetPagesForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            return _unitOfWork.PageRepository.GetManyQueryable(page => page.user_id == userId);

        }

        [Route("user/grades")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<Grade> GetGradesForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            return _unitOfWork.GradeRepository.GetManyQueryable(grade => grade.user_id == userId);
        }

        [Route("user/notifications")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<UserNotification> GetNotificationsForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            return _unitOfWork.UserNotificationRepository.GetManyQueryable(notif => notif.user_id == userId);

        }

        [Route("user/conversations")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<UserConversation> GetConversationsForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            return _unitOfWork.UserConversationRepository.GetManyQueryable(conv => conv.user_id == userId);
        }

        [Route("user/logs")]
        [HttpGet]
        [AuthorizationRequired]
        public IQueryable<Logs> GetLogsForUser()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            return _unitOfWork.LogsRepository.GetManyQueryable(log => log.user_id == userId);
        }

        /*
             * HashPassword
             *
             * Get the hash password for a password
             *
             * @password (string) password string
             * @return (string)
             */
        static string HashPassword(string pasword)
        {
            byte[] arrbyte = new byte[pasword.Length];
            SHA256 hash = new SHA256CryptoServiceProvider();
            arrbyte = hash.ComputeHash(Encoding.UTF8.GetBytes(pasword));
            return Convert.ToBase64String(arrbyte);
        }

        public static string GetRandomSalt()
        {
            int saltSize = 8;
            Random random = new Random();
            byte[] saltBytes = new byte[saltSize];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetNonZeroBytes(saltBytes);
            return Convert.ToBase64String(saltBytes);
        }

        public static byte[] ComputeHash(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            HashAlgorithm hash = new SHA256Managed();
            return hash.ComputeHash(plainTextBytes);
        }

        /*
            * GetFinal
            *
            * Get final string for password
            *
            * @password (string) password string
            * @salt (string) salt string
            * @return (string)
            */
        public static string GetFinal(string password, string salt)
        {
            byte[] passwordHash = ComputeHash(password);
            // string salt =
            //string salt = GetRandomSalt();
            byte[] saltHash = ComputeHash(salt);

            byte[] hashWithSaltBytes = new byte[passwordHash.Length + saltHash.Length];
            for (int i = 0; i < passwordHash.Length; i++)
                hashWithSaltBytes[i] = passwordHash[i];
            for (int i = 0; i < saltHash.Length; i++)
                hashWithSaltBytes[passwordHash.Length + i] = saltHash[i];
            return Convert.ToBase64String(hashWithSaltBytes);
        }

        /*
             * ResetPassword
             *
             * Reset password
             *
             * @value (string) string password
             * @return (IHttpActionResult)
             */
        [Route("resetPass/{value}")]
        [HttpPost]
        [AuthorizationRequired]
        public IHttpActionResult ResetPassword(string value)
        {
            if (value == null || value.Equals(""))
                return BadRequest("Password can't be null");
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user = _unitOfWork.UserRepository.GetByID(userId);
            string Salt = GetRandomSalt();
            user.Salt = Salt;
            user.Password = GetFinal(user.Password, Salt);
            _unitOfWork.UserRepository.Update(user);
            _unitOfWork.Save();
            return Ok();
        }

        /*
            * VerifyPassword
            *
            * Verify a password
            *
            * @value (string) string password
            * @return (IHttpActionResult)
            */
        [Route("verifyPass/{value}")]
        [HttpGet]
        [AuthorizationRequired]
        public IHttpActionResult VerifyPassword(string value)
        {
            if (value == null || value.Equals(""))
                return BadRequest("Password can't be null");
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (_unitOfWork.UserRepository.GetByID(userId).Password.Equals(value))
                return Ok();
            else return BadRequest();
        }

        /*
             * BlockUser
             *
             * Block user with user id
             *
             * @userId (int) user id
             * @return (IHttpActionResult)
             */
        [Route("{userId}/block")]
        [HttpGet]
        [AuthorizationRequired]
        public IHttpActionResult BlockUser(int userId)
        {
            User user = db.Users.Find(userId);
            if (user == null)
                return NotFound();
            user.Blocked = true;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }

        [Route("{userId}/suspend")]
        [HttpGet]
        [AuthorizationRequired]
        public IHttpActionResult SuspendUser(int userId)
        {
            User user = db.Users.Find(userId);
            if (user == null)
                return NotFound();
            user.Suspended = true;
            user.SuspendedUntil = DateTime.Today.AddMonths(1);
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }

        [Route("encode")]
        [HttpGet]
        public IHttpActionResult EncodePasswords()
        {
            foreach (User user in _unitOfWork.UserRepository.GetAll())
            {
                string Salt = GetRandomSalt();
                user.Salt = Salt;
                user.Password = GetFinal(user.Password, Salt);
                _unitOfWork.UserRepository.Update(user);
                
            }
            _unitOfWork.Save();
            return Ok();
        }
    }
}