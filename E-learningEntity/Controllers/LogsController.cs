﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;

namespace E_learningEntity.Controllers
{
    public class LogsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();

        // GET: api/Logs
        [AdminAuthorizationRequired]
        public IQueryable<Logs> GetLogs()
        {
            return db.Logs.OrderByDescending(obj => obj.Date);
        }

        // GET: api/Logs/5
        [ResponseType(typeof(Logs))]
        [AdminAuthorizationRequired]
        public async Task<IHttpActionResult> GetLogs(int id)
        {
            Logs logs = await db.Logs.FindAsync(id);
            if (logs == null)
            {
                return NotFound();
            }

            return Ok(logs);
        }

        // PUT: api/Logs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLogs(int id, Logs logs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != logs.Id)
            {
                return BadRequest();
            }

            db.Entry(logs).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Logs
        [ResponseType(typeof(Logs))]
        public async Task<IHttpActionResult> PostLogs(Logs logs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Logs.Add(logs);
            await db.SaveChangesAsync();

            return Ok(logs);
        }

        // DELETE: api/Logs/5
        [ResponseType(typeof(Logs))]
        public async Task<IHttpActionResult> DeleteLogs(int id)
        {
            Logs logs = await db.Logs.FindAsync(id);
            if (logs == null)
            {
                return NotFound();
            }

            db.Logs.Remove(logs);
            await db.SaveChangesAsync();

            return Ok(logs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogsExists(int id)
        {
            return db.Logs.Count(e => e.Id == id) > 0;
        }
    }
}