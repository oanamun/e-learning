﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("conversations")]
    public class ConversationsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        private UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Conversations/5
        [Route("{id:int}")]
        [HttpGet]
        [ResponseType(typeof(Conversation))]
        [AuthorizationRequired]
        public IHttpActionResult GetConversation(int id)
        {         
            Conversation conversation = db.Conversations.Find(id);
            if (conversation == null)
            {
                return NotFound();
            }

            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (_unitOfWork.UserConversationRepository.Get(conv => conv.user_id == userId && conv.conversation_id == id) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            return Ok(conversation);
        }

        // PUT: api/Conversations/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        [AuthorizationRequired]
        public IHttpActionResult PutConversation(int id, Conversation conversation)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (_unitOfWork.UserConversationRepository.Get(conv => conv.user_id == userId && conv.conversation_id == id) == null)
                return StatusCode(HttpStatusCode.Forbidden);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != conversation.Id)
            {
                return BadRequest();
            }

            db.Entry(conversation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConversationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Conversations
        [ResponseType(typeof(Conversation))]
        [Route("")]
        [HttpPost]
        [AuthorizationRequired]
        public IHttpActionResult PostConversation(Conversation conversation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Conversations.Add(conversation);
            db.SaveChanges();

            return Ok(conversation);
        }

        // DELETE: api/Conversations/5
        [ResponseType(typeof(Conversation))]
        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult DeleteConversation(int id)
        {
            Conversation conversation = db.Conversations.Find(id);
            if (conversation == null)
            {
                return NotFound();
            }            
            foreach (UserConversation conv in db.UserConversations.Where(conv=>conv.conversation_id==conversation.Id).ToList())
            {
                 foreach (Message m in db.Messages.Where(msg=> msg.userConversation_id == conv.Id).ToList())
                {
                    db.Entry(m).State = EntityState.Deleted;
                }
                db.SaveChanges();
                db.Entry(conv).State = EntityState.Deleted;
            }
            db.Entry(conversation).State = EntityState.Deleted;
            db.SaveChanges();
            return Ok(conversation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConversationExists(int id)
        {
            return db.Conversations.Count(e => e.Id == id) > 0;
        }

        //get all user conversation for a conversation
        [Route("{conversationId:int}/UserConversations")]
        [HttpGet]
        public IQueryable<UserConversation> GetUserConversationsByID(int conversationId)
        {
            Conversation conversation = _unitOfWork.ConversationRepository.GetByID(conversationId);
            return _unitOfWork.UserConversationRepository.GetManyQueryable(conv => conv.conversation_id == conversationId);
        }

        [Route("{convId:int}/messages")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult FindMessagesForConversation(int convId)
        {
            Conversation conversation = db.Conversations.Find(convId);
            if (conversation == null)
            {
                return NotFound();
            }

            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (_unitOfWork.UserConversationRepository.Get(conv => conv.user_id == userId && conv.conversation_id == convId) == null)
                return StatusCode(HttpStatusCode.Forbidden);
            List<Message> messages = new List<Message>();
            List<JObject> list = new List<JObject>();
            IQueryable<UserConversation> userConversation = _unitOfWork.UserConversationRepository.GetManyQueryable(conv =>conv.conversation_id == convId);
            foreach (UserConversation conv in userConversation)
                messages.AddRange(_unitOfWork.MessageRepository.GetManyQueryable(message => message.userConversation_id == conv.Id));
            foreach (Message msg in messages.OrderBy(i => i.Date))
            {
                JObject o = new JObject();
                o["Text"] = msg.Text;
                o["Date"] = msg.Date;
                o["Id"] = msg.Id;
                o["Username"] = msg.UserConversation.User.Username;
                list.Add(o);  
            }
            return Ok(list);
        }
    }
}