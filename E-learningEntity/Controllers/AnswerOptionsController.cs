﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using Newtonsoft.Json.Linq;
using E_learningEntity.Services;

namespace E_learningEntity.Controllers
{
    public class AnswerOptionsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();
        
        // GET: api/AnswerOptions/5
        [ResponseType(typeof(JObject))]
        public async Task<IHttpActionResult> GetAnswerOptions(int id)
        {
            AnswerOptions answerOptions = await db.AnswerOptions.FindAsync(id);
            if (answerOptions == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = answerOptions.Id;
            o["Description"] = answerOptions.Description;
            o["question_id"] = answerOptions.question_id;
            return Ok(o);
        }

        // PUT: api/AnswerOptions/5
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutAnswerOptions(int id, AnswerOptions answerOptions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != answerOptions.Id)
            {
                return BadRequest();
            }

            db.Entry(answerOptions).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnswerOptionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AnswerOptions
        [ResponseType(typeof(AnswerOptions))]
        public async Task<IHttpActionResult> PostAnswerOptions(AnswerOptions answerOptions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AnswerOptions.Add(answerOptions);
            await db.SaveChangesAsync();

            return Ok(answerOptions);
        }

        // DELETE: api/AnswerOptions/5
        [ResponseType(typeof(AnswerOptions))]
        public async Task<IHttpActionResult> DeleteAnswerOptions(int id)
        {
            AnswerOptions answerOptions = await db.AnswerOptions.FindAsync(id);
            if (answerOptions == null)
            {
                return NotFound();
            }

            db.AnswerOptions.Remove(answerOptions);
            await db.SaveChangesAsync();

            return Ok(answerOptions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnswerOptionsExists(int id)
        {
            return db.AnswerOptions.Count(e => e.Id == id) > 0;
        }
    }
}