﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using Newtonsoft.Json.Linq;
using E_learningEntity.Services;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("userAnswers")]
    public class UserAnswersController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/UserAnswers
        [Route("")]
        [HttpGet]
        public IQueryable<UserAnswers> GetUserAnswers()
        {
            return db.UserAnswers;
        }

        // GET: api/UserAnswers/5
        [ResponseType(typeof(JObject))]
        public async Task<IHttpActionResult> GetUserAnswers(int id)
        {
            UserAnswers userAnswers = await db.UserAnswers.FindAsync(id);
            if (userAnswers == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = userAnswers.Id;
            o["user_Id"] = userAnswers.user_Id;
            o["Username"] = userAnswers.User.Username;
            o["answerOptionId"] = userAnswers.answerOptionId;
            return Ok(o);
        }

        // PUT: api/UserAnswers/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserAnswers(int id, UserAnswers userAnswers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userAnswers.Id)
            {
                return BadRequest();
            }

            db.Entry(userAnswers).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserAnswersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserAnswers
        [Route("")]
        [HttpPost]
        [ResponseType(typeof(JObject))]
        public async Task<IHttpActionResult> PostUserAnswers(UserAnswers userAnswers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_unitOfWork.UserAnswersRepository.Get(obj => obj.answerOptionId ==userAnswers.answerOptionId && obj.user_Id==userAnswers.user_Id) != null)
                return BadRequest("Raspuns duplicat");

            db.UserAnswers.Add(userAnswers);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["Id"] = userAnswers.Id;
            o["user_Id"] = userAnswers.user_Id;
            o["answerOptionId"] = userAnswers.answerOptionId;
            return Ok(o);
        }

        // DELETE: api/UserAnswers/5
        [Route("{id:int}")]
        [HttpDelete]
        [ResponseType(typeof(UserAnswers))]
        public async Task<IHttpActionResult> DeleteUserAnswers(int id)
        {
            UserAnswers userAnswers = await db.UserAnswers.FindAsync(id);
            if (userAnswers == null)
            {
                return NotFound();
            }

            db.UserAnswers.Remove(userAnswers);
            await db.SaveChangesAsync();

            return Ok(userAnswers);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserAnswersExists(int id)
        {
            return db.UserAnswers.Count(e => e.Id == id) > 0;
        }

    }
}