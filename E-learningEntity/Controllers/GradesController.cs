﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;


namespace E_learningEntity.Controllers
{
    [RoutePrefix("grades")]
    public class GradesController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        private UnitOfWork _unitOfWork = new UnitOfWork();
       
        /*
             * GetAllMyGrades
             *
             * Get all my grades
             *
             * @return (List<JObject>)
             */
        [Route("myGrades")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetAllMyGrades()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            List<JObject> myGrades = new List<JObject>();
            List<int> myCoursesId = new List<int>();

            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId && perm.Role.Equals("Student")))
                if (!myCoursesId.Contains(perm.Course.Id))
                    myCoursesId.Add(perm.Course.Id);
            foreach (int Id in myCoursesId)
            {
                foreach (Module module in _unitOfWork.ModuleRepository.GetManyQueryable(obj => obj.course_id == Id))
                {
                    foreach (Homework homework in _unitOfWork.HomeworkRepository.GetManyQueryable(obj => obj.module_id == module.Id))
                    {
                        Grade gradeH = new Grade();
                        try
                        {
                            gradeH = _unitOfWork.GradeRepository.GetSingle(grade => grade.homework_id == homework.Id && grade.user_id == userId);
                            JObject o = new JObject();
                            o["Value"] = string.Format("{0:0.00}", gradeH.Value); 
                            o["Name"] = homework.Name;
                            o["Module"] = module.Name;
                            o["Type"] = "Tema";
                            myGrades.Add(o);
                        }
                        catch (Exception)
                        {
                            JObject o = new JObject();
                            if (homework.End_date < DateTime.Today)
                            {
                                o["Value"] = 0;
                                o["Name"] = homework.Name;
                                o["Module"] = module.Name;
                                o["Type"] = "Tema";
                                myGrades.Add(o);
                            }
                        }
                    }
                    foreach (Test test in _unitOfWork.TestRepository.GetManyQueryable(obj => obj.module_id == module.Id))
                    {
                        Grade gradeT = new Grade();
                        try
                        {
                            gradeT = _unitOfWork.GradeRepository.GetSingle(grade => grade.test_id == test.Id && grade.user_id == userId);
                            JObject o = new JObject();
                            o["Value"] = string.Format("{0:0.00}", gradeT.Value);
                            o["Name"] = test.Name;
                            o["Module"] = module.Name;
                            o["Type"] = "Test";
                            myGrades.Add(o);
                        }
                        catch (Exception)
                        {
                            JObject o = new JObject();
                            if (test.Due_date < DateTime.Today)
                            {
                                o["Value"] = 0;
                                o["Name"] = test.Name;
                                o["Module"] = module.Name;
                                o["Type"] = "Test";
                                myGrades.Add(o);
                            }
                        }
                    }
                }
                foreach (Discussion discussion in _unitOfWork.DiscussionRepository.GetManyQueryable(obj => obj.course_id == Id))
                {
                    Grade gradeD = new Grade();
                    try
                    {
                        gradeD = _unitOfWork.GradeRepository.GetSingle(grade => grade.discussion_id == discussion.Id && grade.user_id == userId);
                        JObject o = new JObject();
                        o["Value"] = string.Format("{0:0.00}", gradeD.Value);
                        o["Name"] = discussion.Subject;
                        o["Course"] = discussion.Course.Name;
                        o["Type"] = "Discutie";
                        myGrades.Add(o);
                    }
                    catch (Exception)
                    {    }
                }
            }
            return myGrades;
        }

        /*
             * GetFinalGrades
             *
             * Get final grades for a user
             *
             * @return (List<JObject>)
             */
        [Route("finals")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetFinalGrades()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            List<JObject> myGrades = new List<JObject>();
            List<Course> myCoursesId = new List<Course>();
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId && perm.Role.Equals("Student")))
                if (!myCoursesId.Contains(perm.Course))
                    myCoursesId.Add(perm.Course);
            foreach (Course course in myCoursesId)
            {
                try {
                    Grade gradeC = _unitOfWork.GradeRepository.GetSingle(grade => grade.user_id == userId && grade.course_id == course.Id);
                    JObject o = new JObject();
                    o["Value"] = string.Format("{0:0.00}",gradeC.Value);
                    o["course_id"] = course.Id;
                    o["Name"] = gradeC.Course.Name;
                    myGrades.Add(o);
                }
                catch (Exception)
                {
                    JObject o = new JObject();
                    o["Value"] = 0;
                    o["course_id"] = course.Id;
                    o["Name"] = course.Name;
                    myGrades.Add(o);
                }
            }
            return myGrades;
        }

        /*
            * GetGradeStudentCourse
            *
            * Get all course grades for a user
            *
            * @userId (int) user id
            * @return (List<JObject>)
            */
        [Route("{courseId}/{userId}/details")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetGradesStudentCourse(int userId,int courseId)
        {
            List<JObject> myGrades = new List<JObject>();
            foreach (Module module in _unitOfWork.ModuleRepository.GetManyQueryable(obj => obj.course_id == courseId))
            {
                foreach (Homework homework in _unitOfWork.HomeworkRepository.GetManyQueryable(obj => obj.module_id == module.Id))
                {
                    Grade gradeH = new Grade();
                    try
                    {
                        gradeH = _unitOfWork.GradeRepository.GetSingle(grade => grade.homework_id == homework.Id && grade.user_id == userId);
                        JObject o = new JObject();
                        o["Value"] = string.Format("{0:0.00}", gradeH.Value);
                        o["Name"] = homework.Name;
                        o["Module"] = module.Name;
                        o["Type"] = "Tema";
                        myGrades.Add(o);
                    }
                    catch (Exception)
                    {                      
                        if (homework.End_date < DateTime.Today)
                        {
                            JObject o = new JObject();
                            o["Value"] = 0;
                            o["Name"] = homework.Name;
                            o["Module"] = module.Name;
                            o["Type"] = "Tema";
                            myGrades.Add(o);
                        }
                    }
                }
                foreach (Test test in _unitOfWork.TestRepository.GetManyQueryable(obj => obj.module_id == module.Id))
                {
                    Grade gradeT = new Grade();
                    try
                    {
                        gradeT = _unitOfWork.GradeRepository.GetSingle(grade => grade.test_id == test.Id && grade.user_id == userId);
                        JObject o = new JObject();
                        o["Value"] = string.Format("{0:0.00}", gradeT.Value);
                        o["Name"] = test.Name;
                        o["Module"] = module.Name;
                        o["Type"] = "Test";
                        myGrades.Add(o);
                    }
                    catch (Exception)
                    {
                        
                        if (test.Due_date < DateTime.Today)
                        {
                            JObject o = new JObject();
                            o["Value"] = 0;
                            o["Name"] = test.Name;
                            o["Module"] = module.Name;
                            o["Type"] = "Test";
                            myGrades.Add(o);
                        }
                    }
                }
            }
            foreach (Discussion discussion in _unitOfWork.DiscussionRepository.GetManyQueryable(obj => obj.course_id == courseId))
            {
                Grade gradeD = new Grade();
                try
                {
                    gradeD = _unitOfWork.GradeRepository.GetSingle(grade => grade.discussion_id == discussion.Id && grade.user_id == userId);
                    JObject o = new JObject();
                    o["Value"] = string.Format("{0:0.00}", gradeD.Value);
                    o["Name"] = discussion.Subject;
                    o["Type"] = "Discutie";
                    myGrades.Add(o);
                }
                catch (Exception)
                {  }
            }
            return myGrades;
    }


        /*
            * GetFinalGradesForTeacher
            *
            * Get final grades for course id
            *
            * @courseId (int) course id
            * @return (List<JObject>)
            */
        [Route("{courseId:int}/students/finals")]
        [HttpGet]
        [CourseTeacherAuthorizationRequired]
        public List<JObject> GetFinalGradesForTeacher(int courseId)
        {
            List<JObject> myGrades = new List<JObject>();
            List<User> myStudents = new List<User>();
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.course_id==courseId && perm.Role.Equals("Student")))
                if (!myStudents.Contains(perm.User))
                    myStudents.Add(perm.User);
            foreach (User user in myStudents)
            {
                try
                {
                    Grade gradeC = _unitOfWork.GradeRepository.GetSingle(grade => grade.user_id == user.Id && grade.course_id == courseId);
                    JObject o = new JObject();
                    o["Value"] = string.Format("{0:0.00}", gradeC.Value);
                    o["user_id"] = user.Id;
                    o["Username"] = user.Last_name+" "+user.First_name;
                    myGrades.Add(o);
                }
                catch (Exception)
                {
                    JObject o = new JObject();
                        o["Value"] = 0;
                        o["user_id"] = user.Id;
                        o["Username"] = user.Last_name + " " + user.First_name;
                        myGrades.Add(o);
                }
            }
            return myGrades;
        }

        /*
             * GetGrade
             *
             * Get grade for grade id
             *
             * @id (int) grade id
             * @return (Grade)
             */
        [Route("{id:int}", Name="GetGradeById")]
        [HttpGet]
        [ResponseType(typeof(Grade))]
        public async Task<IHttpActionResult> GetGrade(int id)
        {
            Grade grade = await db.Grades.FindAsync(id);
            if (grade == null)
            {
                return NotFound();
            }
            return Ok(grade);
        }

     
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutGrade(int id, Grade grade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != grade.Id)
            {
                return BadRequest();
            }

            db.Entry(grade).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GradeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("", Name="PostGrade")]
        [HttpPost]
        [ResponseType(typeof(Grade))]
        public async Task<IHttpActionResult> PostGrade(Grade grade)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            db.Grades.Add(grade);
            UserNotification notif;
            if (grade.test_id!=null)
              notif = new UserNotification { Seen = false, Message="Ati primit nota: "+grade.Value +" la testul: " +db.Tests.Find(grade.test_id).Name, user_id = grade.user_id };
            else if (grade.homework_id!=null)
                notif = new UserNotification { Seen = false, Message = "Ati primit nota: " + grade.Value + " la tema: " + db.Homeworks.Find(grade.homework_id).Name, user_id = grade.user_id };
            else //notif de discutie
                notif = new UserNotification { Seen = false, Message = "Ati primit nota: " + grade.Value + " la discutia: " + db.Discussions.Find(grade.discussion_id).Subject, user_id = grade.user_id };
            db.UserNotifications.Add(notif);
            await db.SaveChangesAsync();
            return Ok(grade);
        }

        [Route("")]
        [HttpDelete]
        [ResponseType(typeof(Grade))]
        public async Task<IHttpActionResult> DeleteGrade(int id)
        {
            Grade grade = await db.Grades.FindAsync(id);
            if (grade == null)
            {
                return NotFound();
            }

            db.Grades.Remove(grade);
            await db.SaveChangesAsync();

            return Ok(grade);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GradeExists(int id)
        {
            return db.Grades.Count(e => e.Id == id) > 0;
        }


        [Route("tests/{testId:int}/mygrade")]
        [AuthorizationRequired]
        [HttpGet]
        public JObject PostGradeForTest(int testId)
        {
            float Value = 0;
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            IQueryable<Question> questions = _unitOfWork.QuestionRepository.GetManyQueryable(question => question.test_id == testId);
            foreach (Question question in questions)
            {
                IQueryable<AnswerOptions> correctChoices = _unitOfWork.AnswerOptionsRepository.GetManyQueryable(choice => choice.question_id == question.Id && choice.Correct==true);
                int userNrCorrectAnswers = 0;
                foreach (AnswerOptions answer in correctChoices)
                {
                    if (_unitOfWork.UserAnswersRepository.GetManyQueryable(answerU => answerU.user_Id == userId && answerU.answerOptionId == answer.Id).Count() !=0)
                        userNrCorrectAnswers++;
                }
                if (correctChoices.Count() == userNrCorrectAnswers)
                    Value+=(float)question.Pondere*10;
            }
             Grade grade = new Grade { Value = Value, user_id = userId, test_id = testId };
            _unitOfWork.GradeRepository.Add(grade);
            _unitOfWork.Save();
            JObject o = new JObject();
            o["Grade"] = Value;
            return o;
          }


        [Route("tests/{testId:int}/getGrade")]
        [AuthorizationRequired]
        public IHttpActionResult GetGradeForTest(int testId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Grade grade1 = _unitOfWork.GradeRepository.Get(grade => grade.user_id == userId && grade.test_id == testId);
            if (grade1 != null){
                 JObject o = new JObject();
                 o["Grade"] = grade1.Value;
                 return Ok(o);
            }
            else
            {
                JObject o = new JObject();
                o["Grade"] = -1;
                return Ok(o);
            }                
        }

        /*
                   * GetGradeForAHomework
                   *
                   * Get grades for a homework
                   *
                   * @testId (int) homework id
                   * @return (IHttpActionResult)
                   */
        [Route("homeworks/{testId:int}/getGrade")]
        [AuthorizationRequired]
        public IHttpActionResult GetGradeForAHomework(int testId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Grade grade1 = _unitOfWork.GradeRepository.Get(grade => grade.user_id == userId && grade.homework_id == testId);
            if (grade1 != null)
            {
                JObject o = new JObject();
                o["Grade"] = string.Format("{0:0.00}", grade1.Value);
                return Ok(o);
            }
            else
            {
                JObject o = new JObject();
                if (_unitOfWork.HomeworkRepository.GetByID(testId).End_date < DateTime.Today)
                    o["Grade"] = 0;
                return Ok(o);
            }
        }


        [Route("{groupId:int}/{homeworkId:int}/SubmitGrade")]
        [HttpPost]
        [TeacherAuthorizationRequired]
        public IHttpActionResult PostGradeForGroup(Grade grade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IQueryable<User> usersInGroup = _unitOfWork.UserRepository.GetManyQueryable(user => user.group_id == grade.Homework.group_id);
            IQueryable<UserHomeworks> homeworks = _unitOfWork.UserHomeworkRepository.GetManyQueryable(hw => hw.homework_id == grade.homework_id);
            foreach (User user in usersInGroup)
            {
                Grade homeworkGrade = new Grade { homework_id = grade.homework_id, Value = grade.Value, user_id = user.Id };
                _unitOfWork.GradeRepository.Add(homeworkGrade);
                _unitOfWork.Save();
            }
            return Ok();
        }

        /*
            * GetAllGradesToATests
            *
            * Get all grades to for test id
            *
            * @testId (int) test id
            * @return (List<JObject>)
            */
        [Route("tests/{testId}")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetAllGradesToATest(int testId)
        {
            List<JObject> list = new List<JObject>();
            foreach (Grade grade in _unitOfWork.GradeRepository.GetManyQueryable(obj=>obj.test_id==testId))
            {
                    JObject o = new JObject();
                    o["Value"] = string.Format("{0:0.00}", grade.Value);
                    o["User"] = grade.User.Last_name + " " + grade.User.First_name;
                    list.Add(o);
             }
            return list;      
        }

        /*
            * GetAllGradesToAHomework
            *
            * Get all grades for homework id
            *
            * @homeworkId (int) homeWork id
            * @return (List<JObject>)
            */
        [Route("homeworks/{homeworkId}")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetAllGradesToAHomework(int homeworkId)
        {
            List<JObject> list = new List<JObject>();
            foreach (Grade grade in _unitOfWork.GradeRepository.GetManyQueryable(obj => obj.homework_id==homeworkId))
            {
                JObject o = new JObject();
                o["Value"] = string.Format("{0:0.00}", grade.Value);
                o["User"] = grade.User.Last_name + " " + grade.User.First_name;
                list.Add(o);
            }
            return list;
        }

        [Route("discussions/{Id}")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetAllGradesToADiscussion(int Id)
        {
            List<JObject> list = new List<JObject>();
            foreach (Grade grade in _unitOfWork.GradeRepository.GetManyQueryable(obj => obj.discussion_id==Id))
            {
                JObject o = new JObject();
                o["Value"] = string.Format("{0:0.00}", grade.Value);
                o["User"] = grade.User.Last_name + " " + grade.User.First_name;
                list.Add(o);
            }
            return list;
        }


    }
}