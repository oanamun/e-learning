﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;
using E_learningEntity.Services;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("userNotifications")]
    public class UserNotificationsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        [Route("myNotif")]
        [AuthorizationRequired]
        [HttpGet]
        public ICollection<JObject> GetUserNotifications()
        {
            List<JObject> list = new List<JObject>();
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            foreach (UserNotification notif in db.UserNotifications.Where(obj => obj.user_id == userId && obj.Seen == false).ToList())
            {
                JObject o = new JObject();
                o["Id"] = notif.Id;
                o["Message"] = notif.Message;               
                list.Add(o);
            }
            return list;
        }

        // GET: api/UserNotifications/5
        [ResponseType(typeof(UserNotification))]
        public async Task<IHttpActionResult> GetUserNotification(int id)
        {
            UserNotification userNotification = await db.UserNotifications.FindAsync(id);
            if (userNotification == null)
            {
                return NotFound();
            }

            return Ok(userNotification);
        }

        // PUT: api/UserNotifications/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserNotification(int id, UserNotification userNotification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userNotification.Id)
            {
                return BadRequest();
            }

            db.Entry(userNotification).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserNotificationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserNotifications
        [ResponseType(typeof(UserNotification))]
        public async Task<IHttpActionResult> PostUserNotification(UserNotification userNotification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserNotifications.Add(userNotification);
            await db.SaveChangesAsync();

            return Ok(userNotification);
        }

        // DELETE: api/UserNotifications/5
        [ResponseType(typeof(UserNotification))]
        public async Task<IHttpActionResult> DeleteUserNotification(int id)
        {
            UserNotification userNotification = await db.UserNotifications.FindAsync(id);
            if (userNotification == null)
            {
                return NotFound();
            }

            db.UserNotifications.Remove(userNotification);
            await db.SaveChangesAsync();

            return Ok(userNotification);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserNotificationExists(int id)
        {
            return db.UserNotifications.Count(e => e.Id == id) > 0;
        }

        [Route("{id}/seen")]
        [HttpGet]
        [AuthorizationRequired]
        public IHttpActionResult SeenNotification(int id)
        {
            UserNotification notif = db.UserNotifications.Find(id);
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            if (notif.user_id != userId)
                return StatusCode(HttpStatusCode.Forbidden);
            if (notif == null)
                return NotFound();
            notif.Seen = true;
            db.Entry(notif).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }
    }
}