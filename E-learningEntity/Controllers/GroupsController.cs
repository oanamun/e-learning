﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.ActionFilters;
using E_learningEntity.Services;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{

    [RoutePrefix("groups")]
    public class GroupsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Groups
        [Route("")]
        [AdminAuthorizationRequired]
        public ICollection<JObject> GetGroups()
        {
            List<JObject> list = new List<JObject>();
            foreach(Group group in db.Groups)
            {
                JObject o = new JObject();
                o["Id"] = group.Id;
                o["Name"] = group.Name;
                list.Add(o);
            }
            return list;
        }

        // GET: api/Groups/5
        [Route("{id:int}")]
        [ResponseType(typeof(JObject))]
        public IHttpActionResult GetGroup(int id)
        {
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = group.Id;
            o["Name"] = group.Name;
            o["course_id"] = group.course_id;
            return Ok(o);
        }

        // PUT: api/Groups/5
        [Route("{id:int}")]
        [HttpPut]
        [ResponseType(typeof(void))]
        [AdminAuthorizationRequired]
        public IHttpActionResult PutGroup(int id, Group group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != group.Id)
            {
                return BadRequest();
            }

            db.Entry(group).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Groups
        [Route("")]
        [ResponseType(typeof(Group))]
        [AdminAuthorizationRequired]
        public IHttpActionResult PostGroup(Group group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Groups.Add(group);
            db.SaveChanges();

            return Ok(group);
        }

        // DELETE: api/Groups/5
        [Route("")]
        [ResponseType(typeof(Group))]
        [AdminAuthorizationRequired]
        public IHttpActionResult DeleteGroup(int id)
        {
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return NotFound();
            }

            db.Groups.Remove(group);
            db.SaveChanges();

            return Ok(group);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GroupExists(int id)
        {
            return db.Groups.Count(e => e.Id == id) > 0;
        }

        [Route("mygroup")]
        [AuthorizationRequired]
        public JObject GetUserGroup()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Group group= _unitOfWork.GroupRepository.GetByID((int)_unitOfWork.UserRepository.GetByID(userId).group_id);
            JObject o=new JObject();
            o["Id"]=group.Id;
            o["Name"]=group.Name;
            o["course_id"]=group.course_id;
            return o;
        }


        [Route("group/discussions")]
        [HttpGet]
        [ResponseType(typeof(ICollection<JObject>))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> FindDiscussionsForGroup()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];            
            User user = await db.Users.FindAsync(userId);
            if (user.group_id == null)
                return StatusCode(HttpStatusCode.Forbidden);
            IQueryable<Discussion> disc = _unitOfWork.DiscussionRepository.GetManyQueryable(discussion => discussion.group_id == user.group_id);
            List<JObject> list = new List<JObject>();
            foreach (Discussion discussion in disc)
            {
                JObject o = new JObject();
                o["Id"] = discussion.Id;
                o["Subject"] = discussion.Subject;
                o["Description"] = discussion.Description;
                o["Category"] = discussion.Category;
                o["group_id"] = discussion.group_id;
                list.Add(o);
            }
            return Ok(list);

        }

        //get all users for groupId
        [Route("{groupId:int}/users")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetUsersForGroupID(int groupId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user1 = db.Users.Find(userId);
            if (user1.group_id!= groupId)
                return StatusCode(HttpStatusCode.Forbidden);

            List<JObject> list = new List<JObject>();
            foreach (User user in _unitOfWork.UserRepository.GetManyQueryable(user => user.group_id == groupId))
            {
                JObject o = new JObject();
                o["Id"] = user.Id;
                o["Username"] = user.Username;
                o["First_name"] = user.First_name;
                o["Last_name"] = user.Last_name;
                list.Add(o);
            }
            return Ok(list);
        }

        //get all announcements for a group
        [Route("{groupId:int}/announcements")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetAnnouncementsForGroupID(int groupId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user1 = db.Users.Find(userId);
            if (user1.group_id != groupId)
                return StatusCode(HttpStatusCode.Forbidden);
            IQueryable<Announcement> announces = _unitOfWork.AnnouncementRepository.GetManyQueryable(announce => announce.group_id == groupId);
            List<JObject> list = new List<JObject>();
            foreach (Announcement ann in announces)
            {
                JObject o = new JObject();
                o["Id"] = ann.Id;
                o["Title"] = ann.Title;
                o["Message"] = ann.Message;
                o["course_id"] = ann.course_id;
                list.Add(o);
            }
            return Ok(list);
        }

        //get all pages for a group
        [Route("{groupId:int}/pages")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetPagesForGroupID(int groupId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user1 = db.Users.Find(userId);
            if (user1.group_id != groupId)
                return StatusCode(HttpStatusCode.Forbidden);
            IQueryable<Page> pages= _unitOfWork.PageRepository.GetManyQueryable(page => page.group_id == groupId);
           List<JObject> list = new List<JObject>();
            foreach (Page page in pages)
           {
               JObject o = new JObject();
               o["Id"] = page.Id;
               o["Text"] = page.Text;
               o["Title"] = page.Title;
               o["group_id"] = page.group_id;
               o["user_id"] = page.user_id;
               list.Add(o);
           }
            return Ok(list);
        }

        //get all homeworks for a group
        [Route("{groupId:int}/homeworks")]
        [HttpGet]
        [AuthorizationRequired]
        [ResponseType(typeof(ICollection<JObject>))]
        public IHttpActionResult GetHomeworksForGroupID(int groupId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            User user1 = db.Users.Find(userId);
            if (user1.group_id != groupId)
                return StatusCode(HttpStatusCode.Forbidden);
            List<JObject> list = new List<JObject>();
            IQueryable<Homework> homeworks= _unitOfWork.HomeworkRepository.GetManyQueryable(homework => homework.group_id == groupId);
            foreach (Homework homework in homeworks)
            {
                JObject o = new JObject();
                o["Id"] = homework.Id;
                o["Name"] = homework.Name;
                o["Description"] = homework.Description;
                o["Format"] = homework.Format;
                o["End_date"] = homework.End_date;
                o["Grading_type"] = homework.Grading_type;
                o["Required"] = homework.Required;
                o["Pondere"] = homework.Pondere;
                o["group_id"] = homework.group_id;
                list.Add(o);
            }
            return Ok(list);
        
        }

        }
}