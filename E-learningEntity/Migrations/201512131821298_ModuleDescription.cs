namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModuleDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Modules", "Description");
        }
    }
}
