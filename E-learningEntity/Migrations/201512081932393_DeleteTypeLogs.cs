namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteTypeLogs : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Logs", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Logs", "Type", c => c.String());
        }
    }
}
