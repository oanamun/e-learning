namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "Content", c => c.String());
            DropColumn("dbo.Homework", "Start_date");
            DropColumn("dbo.Modules", "description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Modules", "description", c => c.String());
            AddColumn("dbo.Homework", "Start_date", c => c.DateTime(nullable: false));
            DropColumn("dbo.Modules", "Content");
        }
    }
}
