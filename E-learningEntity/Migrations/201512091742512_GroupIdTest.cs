namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupIdTest : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tests", new[] { "group_id" });
            CreateIndex("dbo.Tests", "Group_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tests", new[] { "Group_Id" });
            CreateIndex("dbo.Tests", "group_id");
        }
    }
}
