// <auto-generated />
namespace E_learningEntity.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class userIdEvent : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(userIdEvent));
        
        string IMigrationMetadata.Id
        {
            get { return "201512101613529_userIdEvent"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
