namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectAnswOpt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerOptions", "Correct", c => c.Boolean(nullable: false));
            DropColumn("dbo.Questions", "CorrectAnswer");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Questions", "CorrectAnswer", c => c.String());
            DropColumn("dbo.AnswerOptions", "Correct");
        }
    }
}
