namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Notification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "course_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Notifications", "course_id");
            AddForeignKey("dbo.Notifications", "course_id", "dbo.Courses", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "course_id", "dbo.Courses");
            DropIndex("dbo.Notifications", new[] { "course_id" });
            DropColumn("dbo.Notifications", "course_id");
        }
    }
}
