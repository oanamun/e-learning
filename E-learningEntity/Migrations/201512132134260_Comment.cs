namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Comment : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Comments", new[] { "user_id" });
            AlterColumn("dbo.Comments", "user_id", c => c.Int());
            CreateIndex("dbo.Comments", "user_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "user_id" });
            AlterColumn("dbo.Comments", "user_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Comments", "user_id");
        }
    }
}
