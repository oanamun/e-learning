namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuestionScore : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Questions", "Score");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Questions", "Score", c => c.Int(nullable: false));
        }
    }
}
