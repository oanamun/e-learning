namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Answer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "CorrectAnswer", c => c.String());
            DropColumn("dbo.Questions", "Answer");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Questions", "Answer", c => c.String());
            DropColumn("dbo.Questions", "CorrectAnswer");
        }
    }
}
