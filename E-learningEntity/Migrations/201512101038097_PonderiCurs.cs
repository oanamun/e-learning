namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PonderiCurs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "PondereTests", c => c.Single());
            AddColumn("dbo.Courses", "PondereHomeworks", c => c.Single());
            AddColumn("dbo.Courses", "PondereDiscussions", c => c.Single());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Courses", "PondereDiscussions");
            DropColumn("dbo.Courses", "PondereHomeworks");
            DropColumn("dbo.Courses", "PondereTests");
        }
    }
}
