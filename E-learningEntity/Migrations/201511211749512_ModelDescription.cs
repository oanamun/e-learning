namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Modules", "description");
        }
    }
}
