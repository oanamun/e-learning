﻿namespace E_learningEntity.Migrations
{
    using E_learningEntity.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Numerics;

    internal sealed class Configuration : DbMigrationsConfiguration<E_learningEntity.Models.E_learningEntityContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "E_learningEntity.Models.E_learningEntityContext";
        }

        protected override void Seed(E_learningEntityContext context)
        {

           // seedPacket1(context);
            //seedPacket2(context);
            seedPacket3(context);
            //    seedPacket4(context);
            //  seedPacket5(context);
            //seedPacket6(context);
            // seedPacket7(context);
            // seedPacket8(context);
        }
        private void seedPacket1(E_learningEntityContext context)
        {
            context.Groups.AddOrUpdate(
               new Group { Name = "235" },
               new Group { Name = "236" },
               new Group { Name = "237" },
               new Group { Name = "238" },
               new Group { Name = "239" },
               new Group { Name = "231" },
               new Group { Name = "232" },
               new Group { Name = "233" },
               new Group { Name = "234" }
               );
            context.Courses.AddOrUpdate(
    new Course { Name = "Limbaje formale si tehnici de compilare", Description = "Aplicarea tehnicilor de compilare pentru diferite probleme din viata reala", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 2, PondereHomeworks = 3, PondereTests = 5 },
    new Course { Name = "Programare web", Description = "Familiarizarea studentilor cu principalele concepte, tehnologii atât client side cât si server side precum si cu instrumentele cel mai des folosite in programarea Web.", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 1, PondereHomeworks = 4, PondereTests = 5 },
    new Course { Name = "Calcul numeric", Description = "Aplicarea Analizei numerice la rezolvarea problemelor din lumea reala pe calculator", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 2, PondereHomeworks = 2, PondereTests = 6 },
    new Course { Name = "Baze de date", Description = "Cunoasterea modelelor de descriere a datelor, in special al modelului relational.", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 2, PondereHomeworks = 2, PondereTests = 6 },
    new Course { Name = "Baze de date distribuite", Description = "Aprofundarea de cunostinte legate de gestiunea datelor si a obiectelor pe un server de baze de date", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 2, PondereHomeworks = 2, PondereTests = 6 },
    new Course { Name = "Retele de calculatoare", Description = "insusirea de catre cursant a principiilor fundamentale care stau la baza functionarii unei retele de calculatoare in particular si a retelei Internet in general", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 3, PondereHomeworks = 2, PondereTests = 5 },
    new Course { Name = "Programare logica si functionala", Description = "Deprinderea studentului cu paradigma programarii declarative (programarea functionala si programarea logica).", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 1, PondereHomeworks = 2, PondereTests = 7 },
    new Course { Name = "Analiza matematica", Description = "Aprofundarea cunostintelor clasice de calcul diferential si integral pentru functii reale si vectoriale de una sau mai multe variabile reale si sa le aplice in situatii precizate.", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 2, PondereHomeworks = 4, PondereTests = 4 },
    new Course { Name = "Sisteme de operare distribuite", Description = "Bazele programarii concurente la nivel de threaduri;Bazele comunicarii prin socket intre sisteme, cu aplicatii la Unix si Windows. ", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 3, PondereHomeworks = 1, PondereTests = 6 },
    new Course { Name = "Design patterns", Description = "Design si implementare de patterns in orice limbaj de programare orientat obicet.", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 4, PondereHomeworks = 6 },
    new Course { Name = "Grafica pe calculator", Description = "Implementare si desing de obiecte 2D si 3D.Stereograme.Anaglife.", Department = "Informatica", Start_date = DateTime.Today, End_date = new DateTime(2017, 09, 10), PondereDiscussions = 3, PondereHomeworks = 6, PondereTests = 1 }


);
        }


        private void seedPacket2(E_learningEntityContext context)
        {
            context.Users.AddOrUpdate(
               new User { Username = "admin", Password = "pass", First_name = "Administrator", Last_name = "Domnul", Email = "adminubb@gmail.com" },
               new User { Username = "oanam", Password = "pass", First_name = "Oana", Last_name = "Muntean", Email = "oanamuntean8@gmail.com", Registration_number = 1234, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = true, Student = true },
               new User { Username = "elizan", Password = "pass", First_name = "Eliza", Last_name = "Nitoi", Email = "elizanitoi@gmail.com", Registration_number = 1241, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = true, Student = true },
               new User { Username = "oanap", Password = "pass", First_name = "Oana", Last_name = "Petrean", Email = "oona1994@yahoo.com", Registration_number = 1232, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = true, Student = true },
               new User { Username = "alexr", Password = "pass", First_name = "Alex", Last_name = "Ruja", Email = "alex.ruja1@gmail.com", Registration_number = 1233, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false, Student = true },
               new User { Username = "roxanas", Password = "pass", First_name = "Roxana", Last_name = "Spataru", Email = "roxanaspataru@gmail.com", Registration_number = 1242, Suspended = true, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = true, Student = true },
               new User { Username = "andic", Password = "pass", First_name = "Andi", Last_name = "Coman", Email = "comanandi@gmail.com", Registration_number = 1243, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false, Student = true },
               new User { Username = "benim", Password = "pass", First_name = "Beniamin", Last_name = "Muntean", Email = "beniaminmuntean@gmail.com", Registration_number = 1263, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "emiliap", Password = "pass", First_name = "Emilia", Last_name = "Pop", Email = "pop_emilia@yahoo.com", Registration_number = 1244, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "arianat", Password = "pass", First_name = "Ariana", Last_name = "Todea", Email = "ariana_todea@yahoo.com", Registration_number = 1245, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "alexandrasa", Password = "pass", First_name = "Alexandra", Last_name = "Sabo", Email = "alecsandra_sabo@yahoo.com", Registration_number = 1246, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "evet", Password = "pass", First_name = "Evelyne", Last_name = "Todesc", Email = "eve_tod@yahoo.com", Registration_number = 1247, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "pojara", Password = "pass", First_name = "Alexandru", Last_name = "Pojar", Email = "pojar_alexandru@yahoo.com", Registration_number = 1248, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "cristip", Password = "pass", First_name = "Cristi", Last_name = "Pop", Email = "cristi.pop@yahoo.com", Registration_number = 1249, Suspended = false, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "vladd", Password = "pass", First_name = "Vlad", Last_name = "Dragos", Email = "dragos_vlad@yahoo.com", Registration_number = 1250, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "razvanm", Password = "pass", First_name = "Razvan", Last_name = "Mos", Email = "mos_razvan@yahoo.com", Registration_number = 1251, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id, Gender = false, Student = true },
               new User { Username = "larisaz", Password = "pass", First_name = "Larisa", Last_name = "Zah", Email = "zah_larisa@yahoo.com", Registration_number = 1252, Suspended = true, Blocked = false, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false, Student = true },
               new User { Username = "biancap", Password = "pass", First_name = "Bianca", Last_name = "Popescu", Email = "popescu.bianca@yahoo.com", Registration_number = 1253, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false, Student = true },
               new User { Username = "cerbu", Password = "pass", First_name = "Mihai", Last_name = "Popa", Email = "popa_mihai@yahoo.com", Registration_number = 1254, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "alexandras", Password = "pass", First_name = "Alexandraa", Last_name = "Seghedi", Email = "seghedi_alexandra@yahoo.com", Registration_number = 1255, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "ancas", Password = "pass", First_name = "Anca", Last_name = "Sarbu", Email = "sarbu_anca@yahoo.com", Registration_number = 1256, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "arianap", Password = "pass", First_name = "Ariana", Last_name = "Puscas", Email = "ariana_puscas@yahoo.com", Registration_number = 1257, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "roxanar", Password = "pass", First_name = "Roxana", Last_name = "Rusu", Email = "roxana_rusu@yahoo.com", Registration_number = 1258, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "adrianaa", Password = "pass", First_name = "Adriana", Last_name = "Ardelean", Email = "adriana_ardelean@yahoo.com", Registration_number = 1259, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "cristianas", Password = "pass", First_name = "Cristina", Last_name = "Sandru", Email = "cristina_sandru@yahoo.com", Registration_number = 1260, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "alexj", Password = "pass", First_name = "Alex", Last_name = "Jurja", Email = "pop_emilia@yahoo.com", Registration_number = 1261, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "iulianp", Password = "pass", First_name = "Iulian", Last_name = "Popescu", Email = "iulian_popescu@yahoo.com", Registration_number = 1262, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "razvans", Password = "pass", First_name = "Razvan", Last_name = "Stetcu", Email = "stetcu.razvan@yahoo.com", Registration_number = 1262, Suspended = true, Blocked = true, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id, Gender = false },
               new User { Username = "alexandruv", Password = "pass", First_name = "Alexandru", Last_name = "Vancea", Email = "alvancea@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "czibulag", Password = "pass", First_name = "Gabriela", Last_name = "Czibula", Email = "czibulagab@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "ioanl", Password = "pass", First_name = "Ioan", Last_name = "Lazar", Email = "lazar@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "ancaa", Password = "pass", First_name = "Anca", Last_name = "Andreica", Email = "ancaandr@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "raresb", Password = "pass", First_name = "Rares", Last_name = "Boian", Email = "raresboi@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "sandad", Password = "pass", First_name = "Sanda", Last_name = "Dragos", Email = "sandaa@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "radug", Password = "pass", First_name = "Ragu", Last_name = "Gaceanu", Email = "radug@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "per", Password = "pass", First_name = "Vasile", Last_name = "Prejmerean", Email = "per@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "andreean", Password = "pass", First_name = "Andreea", Last_name = "Navroschi", Email = "andreeanav@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "mihoct", Password = "pass", First_name = "Tudor", Last_name = "Mihoc", Email = "mihoctudor@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "bradu", Password = "pass", First_name = "Radu", Last_name = "Dragos", Email = "radudragos@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "danc", Password = "pass", First_name = "Dan", Last_name = "Cojocar", Email = "dancojocar@cs.ubbcluj.ro", Suspended = false, Blocked = false },
               new User { Username = "bufny", Password = "pass", First_name = "Darius", Last_name = "Bufnea", Email = "dariusbufnea@cs.ubbcluj.ro", Suspended = false, Blocked = false }



                 );

            context.Modules.AddOrUpdate(
                     new Module { Name = "Gramatici si limbaje", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id },
                     new Module { Name = "Limbaje regulare", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id },
                     new Module { Name = "Gramatici independente de context (GIC)", Start_date = new DateTime(2015, 10, 15), course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id },
                     new Module { Name = "Introducere in HTML. Structura unui document HTML.Taguri de baza.", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id },
                     new Module { Name = "Introducere in CSS", Start_date = new DateTime(2015, 10, 20), course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id },
                     new Module { Name = "Introducere in JavaScript/JQuery", Start_date = new DateTime(2015, 10, 25), course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id },
                     new Module { Name = "Obiectivele, problematica si metodele Analizei numerice.Formula lui Taylor. ", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id },
                     new Module { Name = "Aritmetica in virgula flotanta. Conditionarea unei probleme.Stabilitatea algoritmilor numerici", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id },
                     new Module { Name = "Sisteme de ecuatii liniare. Conditionare", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id },
                     new Module { Name = "Conceptele bazelor de date", Start_date = new DateTime(2015, 10, 20), course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id },
                     new Module { Name = "Modelul relational de organizare a bazelor de date", Start_date = new DateTime(2015, 10, 25), course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id },
                     new Module { Name = "Interogarea bazelor de date", Start_date = new DateTime(2015, 11, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id },
                     new Module { Name = "Tranzactia. Arhitectura unei tranzactii. Controlul concurentei.Plan de executie.", Start_date = new DateTime(2015, 10, 30), course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id },
                     new Module { Name = "Anomalii de interferenta. Istorie recuperabila, evitarea anularilor in cascada, executie stricta.Serializabilitate.", Start_date = new DateTime(2015, 11, 05), course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id },
                     new Module { Name = "Gestiunea interblocarilor", Start_date = new DateTime(2015, 11, 10), course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id },
                     new Module { Name = "Introducere in retele de calculatoare. Definitie. Exemple.Topologii de retele.", Start_date = new DateTime(2015, 10, 10), course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id },
                     new Module { Name = "Recapitulare interfata socket()", Start_date = new DateTime(2015, 10, 15), course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id },
                     new Module { Name = "Notiunea de protocol. Stive de protocoale. Modelul OSI si TCP / IP.", Start_date = new DateTime(2015, 10, 20), course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id },
                     new Module { Name = "Programare si limbaje de programare. Programare imperativa vs.programare declarativa.Introducere. Recursivitate.Exemple", Start_date = new DateTime(2015, 10, 25), course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                     new Module { Name = "Elemente fundamentale ale limbajului Prolog. Fapte si reguli Prolog", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                     new Module { Name = "Programul Prolog", Start_date = new DateTime(2015, 10, 05), course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                     new Module { Name = "Siruri de numere reale", Start_date = new DateTime(2015, 10, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Analiza matematica").Id },
                     new Module { Name = "Serii de numere reale", Start_date = new DateTime(2015, 10, 05), course_id = context.Courses.FirstOrDefault(p => p.Name == "Analiza matematica").Id },
                     new Module { Name = "Calcul diferential pe axa reala", Start_date = new DateTime(2015, 10, 10), course_id = context.Courses.FirstOrDefault(p => p.Name == "Analiza matematica").Id },
                     new Module { Name = "Procese si IPC in sistemele Windows.", Start_date = new DateTime(2015, 12, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id },
                     new Module { Name = "Multiprocesare si concurenta folosind threaduri.", Start_date = new DateTime(2015, 12, 05), course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id },
                     new Module { Name = "Layers", Start_date = new DateTime(2016, 01, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id },
                     new Module { Name = "Pipes and filters", Start_date = new DateTime(2016, 01, 02), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Client-Server", Start_date = new DateTime(2016, 01, 03), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Single Responsibility Principle", Start_date = new DateTime(2016, 01, 04), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Factory Method", Start_date = new DateTime(2016, 01, 05), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Abstract Factory", Start_date = new DateTime(2016, 01, 06), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Builder", Start_date = new DateTime(2016, 01, 07), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Prototype", Start_date = new DateTime(2016, 01, 08), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Singleton", Start_date = new DateTime(2016, 01, 10), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Adapter", Start_date = new DateTime(2016, 01, 11), course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
                     new Module { Name = "Grafica 2D", Start_date = new DateTime(2016, 02, 01), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Transformari geometrice", Start_date = new DateTime(2016, 02, 02), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Proiectii", Start_date = new DateTime(2016, 02, 03), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Reprezentarea curbelor", Start_date = new DateTime(2016, 02, 04), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Reprezentarea suprafetelor", Start_date = new DateTime(2016, 02, 05), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Reprezentarea corpurilor", Start_date = new DateTime(2016, 02, 06), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Decuparea", Start_date = new DateTime(2016, 02, 08), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Aducerea observatorului pe OZ", Start_date = new DateTime(2016, 02, 09), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Modelarea corpurilor", Start_date = new DateTime(2016, 02, 10), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Cresterea realismului imaginilor", Start_date = new DateTime(2016, 02, 11), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Texturi", Start_date = new DateTime(2016, 02, 12), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Lumină şi umbră", Start_date = new DateTime(2016, 02, 13), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Anaglife", Start_date = new DateTime(2016, 02, 14), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
                     new Module { Name = "Stereograme", Start_date = new DateTime(2016, 02, 15), course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id }
                     );
        }


        private void seedPacket3(E_learningEntityContext context)
        {
            context.Permissions.AddOrUpdate(

                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "benim").Id },
                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },
                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },
                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },
                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },
                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },

                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },

                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },



                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },

                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "emiliap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianat").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandrasa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "evet").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "pojara").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "vladd").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvanm").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "adrianaa").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexj").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "iulianp").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "razvans").Id },
                new Permission { Role = "Student", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "roxanas").Id },




                //new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "benim").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "benim").Id },
                new Permission { Role = "Asistent", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "bradu").Id },
                new Permission { Role = "Asistent", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "mihoct").Id },
                new Permission { Role = "Asistent", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "danc").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "bradu").Id },
                new Permission { Role = "Asistent", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare Web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "radug").Id },
                new Permission { Role = "Asistent", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare Web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "sandad").Id },
                new Permission { Role = "Asistent", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare Web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "ancaa").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare Web").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "bufny").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "alexandruv").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "andreean").Id },
                new Permission { Role = "Profesor", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, user_id = context.Users.FirstOrDefault(p => p.Username == "czibulag").Id },

                new Permission { Role = "Admin", course_id = null, user_id = context.Users.FirstOrDefault(p => p.Username == "admin").Id }
);

            context.Tests.AddOrUpdate(

                        new Test
                        {
                            Name = "Test Gramatici si limbaje",
                            Description = "30% din seminar",
                            Due_date = new DateTime(2015, 12, 01),
                            Type = "obligatoriu",
                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Gramatici si limbaje").Id
                        },
                        new Test
                        {
                            Name = "Test Limbaje regulare",
                            Description = "2% din nota finala",
                            Due_date = new DateTime(2015, 12, 05),
                            Type = "optional",
                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Limbaje regulare").Id
                        },

                    new Test
                    {
                        Name = "Test Gramatici independente de context (GIC)",
                        Description = "3% din nota finala",
                        Due_date = DateTime.Today,
                        Type = "obligatoriu",
                        module_id = context.Modules.FirstOrDefault(p => p.Name == "Gramatici independente de context (GIC)").Id
                    },
                 new Test
                 {
                     Name = "Test Introducere in HTML. Structura unui document HTML.Taguri de baza.",
                     Description = "4% din nota finala",
                     Due_date = DateTime.Today,
                     Type = "optional",
                     module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in HTML. Structura unui document HTML.Taguri de baza.").Id
                 },
                 new Test
                 {
                     Name = "Test Introducere in CSS",
                     Description = "5% din nota finala",
                     Due_date = DateTime.Today,
                     Type = "optional",
                     module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in CSS").Id
                 },
                 new Test
                 {
                     Name = "Test Introducere in JavaScript/JQuery",
                     Description = "5% din nota finala",
                     Due_date = DateTime.Today,
                     Type = "obligatoriu",
                     module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in JavaScript/JQuery").Id
                 },
                 new Test
                 {
                     Name = "Test Obiectivele, problematica si metodele Analizei numerice.Formula lui Taylor.",
                     Description = "2% din nota finala",
                     Due_date = new DateTime(2015, 12, 06),
                     Type = "optional",
                     module_id = context.Modules.FirstOrDefault(p => p.Name == "Obiectivele, problematica si metodele Analizei numerice.Formula lui Taylor.").Id
                 },
                                       new Test
                                       {
                                           Name = "Test Aritmetica in virgula flotanta. Conditionarea unei probleme.Stabilitatea algoritmilor numerici",
                                           Description = "20% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "obligatoriu",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Aritmetica in virgula flotanta. Conditionarea unei probleme.Stabilitatea algoritmilor numerici").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Sisteme de ecuatii liniare. Conditionare",
                                           Description = "30% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "obligatoriu",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Sisteme de ecuatii liniare. Conditionare").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Conceptele bazelor de date",
                                           Description = "25% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "obligatoriu",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Conceptele bazelor de date").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Modelul relational de organizare a bazelor de date",
                                           Description = "10% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "optional",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Modelul relational de organizare a bazelor de date").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Interogarea bazelor de date",
                                           Description = "2% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "optional",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Interogarea bazelor de date").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Tranzactia. Arhitectura unei tranzactii. Controlul concurentei.Plan de executie.",
                                           Description = "10% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "obligatoriu",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Tranzactia. Arhitectura unei tranzactii. Controlul concurentei.Plan de executie.").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Anomalii de interferenta. Istorie recuperabila, evitarea anularilor in cascada, executie stricta.Serializabilitate.",
                                           Description = "2% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "optional",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Anomalii de interferenta. Istorie recuperabila, evitarea anularilor in cascada, executie stricta.Serializabilitate.").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Gestiunea interblocarilor",
                                           Description = "1% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "optional",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Gestiunea interblocarilor").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Introducere in retele de calculatoare. Definitie. Exemple.Topologii de retele.",
                                           Description = "20% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "obligatoriu",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in retele de calculatoare. Definitie. Exemple.Topologii de retele.").Id
                                       },
                                        new Test
                                        {
                                            Name = "Test Recapitulare interfata socket()",
                                            Description = "2% din nota finala",
                                            Due_date = DateTime.Today,
                                            Type = "optional",
                                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Recapitulare interfata socket()").Id
                                        },
                                       new Test
                                       {
                                           Name = "Test Notiunea de protocol. Stive de protocoale. Modelul OSI si TCP / IP.",
                                           Description = "10% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "obligatoriu",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Notiunea de protocol. Stive de protocoale. Modelul OSI si TCP / IP.").Id
                                       },
                                       new Test
                                       {
                                           Name = "Test Programare si limbaje de programare. Programare imperativa vs.programare declarativa.Introducere. Recursivitate.Exemple",
                                           Description = "2% din nota finala",
                                           Due_date = DateTime.Today,
                                           Type = "optional",
                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Programare si limbaje de programare. Programare imperativa vs.programare declarativa.Introducere. Recursivitate.Exemple").Id

                                       },
                                              new Test
                                              {
                                                  Name = "Test Elemente fundamentale ale limbajului Prolog. Fapte si reguli Prolog",
                                                  Description = "10% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Elemente fundamentale ale limbajului Prolog. Fapte si reguli Prolog").Id
                                              },
                                              new Test
                                              {
                                                  Name = "Test Programul Prolog",
                                                  Description = "20% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Programul Prolog").Id
                                              },
                                              new Test
                                              {
                                                  Name = "Test Siruri de numere reale",
                                                  Description = "10% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Siruri de numere reale").Id
                                              },
                                              new Test
                                              {
                                                  Name = "Test Serii de numere reale",
                                                  Description = "10% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Serii de numere reale").Id
                                              },
                                              new Test
                                              {
                                                  Name = "Test Calcul diferential pe axa reala",
                                                  Description = "20% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Calcul diferential pe axa reala").Id
                                              },
                                               new Test
                                               {
                                                   Name = "Test Procese si IPC in sistemele Windows.",
                                                   Description = "5% din nota finala",
                                                   Due_date = DateTime.Today,
                                                   Type = "optional",
                                                   module_id = context.Modules.FirstOrDefault(p => p.Name == "Procese si IPC in sistemele Windows.").Id
                                               },
                                              new Test
                                              {
                                                  Name = "Test Multiprocesare si concurenta folosind threaduri.",
                                                  Description = "10% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Multiprocesare si concurenta folosind threaduri.").Id
                                              },

                        new Test
                        {
                            Name = "Test Layers",
                            Description = "2% din nota finala",
                            Due_date = DateTime.Today,
                            Type = "obligatoriu",
                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Layers").Id
                        },
                            new Test
                            {
                                Name = "Test Pipes and filters",
                                Description = "3% din nota finala",
                                Due_date = DateTime.Today,
                                Type = "obligatoriu",
                                module_id = context.Modules.FirstOrDefault(p => p.Name == "Pipes and filters").Id
                            },
                                    new Test
                                    {
                                        Name = "Test Client-Server",
                                        Description = "2% din nota finala",
                                        Due_date = DateTime.Today,
                                        Type = "obligatoriu",
                                        module_id = context.Modules.FirstOrDefault(p => p.Name == "Client-Server").Id
                                    },
                                            new Test
                                            {
                                                Name = "Test Single Responsibility Principle",
                                                Description = "3% din nota finala",
                                                Due_date = DateTime.Today,
                                                Type = "obligatoriu",
                                                module_id = context.Modules.FirstOrDefault(p => p.Name == "Single Responsibility Principle").Id
                                            },
                                                    new Test
                                                    {
                                                        Name = "Test Factory Method",
                                                        Description = "2% din nota finala",
                                                        Due_date = DateTime.Today,
                                                        Type = "obligatoriu",
                                                        module_id = context.Modules.FirstOrDefault(p => p.Name == "Factory Method").Id
                                                    },
                                                            new Test
                                                            {
                                                                Name = "Test Abstract Factory",
                                                                Description = "2% din nota finala",
                                                                Due_date = DateTime.Today,
                                                                Type = "obligatoriu",
                                                                module_id = context.Modules.FirstOrDefault(p => p.Name == "Abstract Factory").Id
                                                            },

                                   new Test
                                   {
                                       Name = "Test Builder",
                                       Description = "2% din nota finala",
                                       Due_date = DateTime.Today,
                                       Type = "obligatoriu",
                                       module_id = context.Modules.FirstOrDefault(p => p.Name == "Builder").Id
                                   },
                                      new Test
                                      {
                                          Name = "Test Prototype",
                                          Description = "2% din nota finala",
                                          Due_date = DateTime.Today,
                                          Type = "obligatoriu",
                                          module_id = context.Modules.FirstOrDefault(p => p.Name == "Prototype").Id
                                      },
                                         new Test
                                         {
                                             Name = "Test Singleton",
                                             Description = "2% din nota finala",
                                             Due_date = DateTime.Today,
                                             Type = "obligatoriu",
                                             module_id = context.Modules.FirstOrDefault(p => p.Name == "Singleton").Id
                                         },
                                            new Test
                                            {
                                                Name = "Test Adapter",
                                                Description = "2% din nota finala",
                                                Due_date = DateTime.Today,
                                                Type = "obligatoriu",
                                                module_id = context.Modules.FirstOrDefault(p => p.Name == "Adapter").Id
                                            },
                                              new Test
                                              {
                                                  Name = "Test Grafica 2D",
                                                  Description = "2% din nota finala",
                                                  Due_date = DateTime.Today,
                                                  Type = "obligatoriu",
                                                  module_id = context.Modules.FirstOrDefault(p => p.Name == "Grafica 2D").Id
                                              },
                                                   new Test
                                                   {
                                                       Name = "Test Transformari geometrice",
                                                       Description = "2% din nota finala",
                                                       Due_date = DateTime.Today,
                                                       Type = "obligatoriu",
                                                       module_id = context.Modules.FirstOrDefault(p => p.Name == "Transformari geometrice").Id
                                                   },
                                                        new Test
                                                        {
                                                            Name = "Test Proiectii",
                                                            Description = "2% din nota finala",
                                                            Due_date = DateTime.Today,
                                                            Type = "obligatoriu",
                                                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Proiectii").Id
                                                        },

                                                             new Test
                                                             {
                                                                 Name = "Test Reprezentarea curbelor",
                                                                 Description = "2% din nota finala",
                                                                 Due_date = DateTime.Today,
                                                                 Type = "obligatoriu",
                                                                 module_id = context.Modules.FirstOrDefault(p => p.Name == "Reprezentarea curbelor").Id
                                                             },
                                                                  new Test
                                                                  {
                                                                      Name = "Test Reprezentarea suprafetelor",
                                                                      Description = "2% din nota finala",
                                                                      Due_date = DateTime.Today,
                                                                      Type = "obligatoriu",
                                                                      module_id = context.Modules.FirstOrDefault(p => p.Name == "Reprezentarea suprafetelor").Id
                                                                  },
                                                                   new Test
                                                                   {
                                                                       Name = "Test Reprezentarea corpurilor",
                                                                       Description = "2% din nota finala",
                                                                       Due_date = DateTime.Today,
                                                                       Type = "obligatoriu",
                                                                       module_id = context.Modules.FirstOrDefault(p => p.Name == "Reprezentarea corpurilor").Id
                                                                   },
                                                                    new Test
                                                                    {
                                                                        Name = "Test Decuparea",
                                                                        Description = "2% din nota finala",
                                                                        Due_date = DateTime.Today,
                                                                        Type = "obligatoriu",
                                                                        module_id = context.Modules.FirstOrDefault(p => p.Name == "Decuparea").Id
                                                                    },
                                                                     new Test
                                                                     {
                                                                         Name = "Test Aducerea observatorului pe OZ",
                                                                         Description = "2% din nota finala",
                                                                         Due_date = DateTime.Today,
                                                                         Type = "obligatoriu",
                                                                         module_id = context.Modules.FirstOrDefault(p => p.Name == "Aducerea observatorului pe OZ").Id
                                                                     },
                                                                      new Test
                                                                      {
                                                                          Name = "Test Modelarea corpurilor",
                                                                          Description = "2% din nota finala",
                                                                          Due_date = DateTime.Today,
                                                                          Type = "obligatoriu",
                                                                          module_id = context.Modules.FirstOrDefault(p => p.Name == "Modelarea corpurilor").Id
                                                                      },
                                                                       new Test
                                                                       {
                                                                           Name = "Test Cresterea realismului imaginilor",
                                                                           Description = "2% din nota finala",
                                                                           Due_date = DateTime.Today,
                                                                           Type = "obligatoriu",
                                                                           module_id = context.Modules.FirstOrDefault(p => p.Name == "Cresterea realismului imaginilor").Id
                                                                       },
                                                                        new Test
                                                                        {
                                                                            Name = "Test Texturi",
                                                                            Description = "2% din nota finala",
                                                                            Due_date = DateTime.Today,
                                                                            Type = "obligatoriu",
                                                                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Texturi").Id
                                                                        },
                                                                                new Test
                                                                                {
                                                                                    Name = "Test Lumină şi umbră",
                                                                                    Description = "2% din nota finala",
                                                                                    Due_date = DateTime.Today,
                                                                                    Type = "obligatoriu",
                                                                                    module_id = context.Modules.FirstOrDefault(p => p.Name == "Lumină şi umbră").Id
                                                                                },

                                                                                        new Test
                                                                                        {
                                                                                            Name = "Test Anaglife",
                                                                                            Description = "2% din nota finala",
                                                                                            Due_date = DateTime.Today,
                                                                                            Type = "obligatoriu",
                                                                                            module_id = context.Modules.FirstOrDefault(p => p.Name == "Anaglife").Id
                                                                                        },
                                                                                                new Test
                                                                                                {
                                                                                                    Name = "Test Stereograme",
                                                                                                    Description = "2% din nota finala",
                                                                                                    Due_date = DateTime.Today,
                                                                                                    Type = "obligatoriu",
                                                                                                    module_id = context.Modules.FirstOrDefault(p => p.Name == "Stereograme").Id
                                                                                                }
                        );


            context.Announcements.AddOrUpdate(
               f => f.Id,
                new Announcement { Title = "Voi absenta la urmatorul curs", Message = "Salut dragi studenti, voi absenta la urmatorul curs. Ne vom intelege saptamana urmatoare pentru cand vom putea recupera. Sa va anuntati si restul colegilor. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id },
                new Announcement { Title = "Laborator Programare Web", Message = "Salut! Buna. Din cauza unor probleme personale nu o sa pot sa ajung astazi la laborator. V-as ruga sa gasiti o zi si o ora de la care v-ar fi ok sa recuperam seminarul si sa ma anuntati.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id },
                new Announcement { Title = "Deadline test PW", Message = "S-a modificat deadline-ul pentru testul 2. Verificati-va notificarile.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Analiza matematica").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Recuperare laborator", Message = "Buna! Recuperam laboratorul pierdut saptamana aceasta, joi de la 10:00. O sa va anunt sala in zilele urmatoare.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id },
                new Announcement { Title = "Recuperare laborator 2.0", Message = "Am gasit sala. Joi la 10:00 in sala C310. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id },
                new Announcement { Title = "Practic", Message = "Salut! Din cauza ca eu voi fi plecat in ultima saptamana, vom tine practicul in penultima saptamana, la laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Recuperare laborator 3.0", Message = "Am gasit sala. Joi la 10:00 in sala C333. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id },
                new Announcement { Title = "Recuperare laborator 4.0", Message = "Am gasit sala. Joi la 10:00 in sala C339. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                new Announcement { Title = "Recuperare laborator 1.0", Message = "Am gasit sala. Joi la 10:00 in sala C339. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                new Announcement { Title = "Recuperare laborator 6.0", Message = "Am gasit sala. Joi la 10:00 in sala C339. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                new Announcement { Title = "Recuperare laborator 14.0", Message = "Am gasit sala. Joi la 10:00 in sala C339. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
                new Announcement { Title = "Recuperare laborator 5.0", Message = "Am gasit sala. Joi la 10:00 in sala C339. O zi buna!", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Limbaje formale si tehnici de compilare").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Retele de calculatoare").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Calcul numeric").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
                new Announcement { Title = "Nu se tine urmatorul seminar", Message = "Buna! Nu pot sa ajung la urmatorul seminar. O zi placuta! :)", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Practic", Message = "Salut!Practicul se tine in ultima saptamana de laborator.", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "231").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "232").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "233").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "234").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "237").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "238").Id },
                new Announcement { Title = "Secret Santa!", Message = "Salut! :) Nu uitati, la urmatorul laborator se va desfasura Secret Santa!", group_id = context.Groups.FirstOrDefault(p => p.Name == "239").Id }

);

            context.Discussions.AddOrUpdate(
               new Discussion { Subject = "Libraria JQuery", Description = "La ce ne foloseste JQuery", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id },
               new Discussion { Subject = "Proiect Colectiv", Description = "Cum implementati backend-ul", group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
               new Discussion { Subject = "Practic", Description = "Lucruri utile pentru Practic", group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },
               new Discussion { Subject = "Exercitii Analiza matematica", Description = "Modele si rezolvari de probleme", group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },
               new Discussion { Subject = "Recapitulare", Description = "Recapitulare Serii de numere reale", course_id = context.Courses.FirstOrDefault(p => p.Name == "Analiza matematica").Id },
               new Discussion { Subject = "Socket-uri", Description = "La ce ne ajuta socket-urile?", course_id = context.Courses.FirstOrDefault(p => p.Name == "Sisteme de operare distribuite").Id },
               new Discussion { Subject = "Interblocari", Description = "Cum functioneaza interblocarile", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date distribuite").Id },
               new Discussion { Subject = "EntityFramework", Description = "La ce ne foloseste EF", course_id = context.Courses.FirstOrDefault(p => p.Name == "Baze de date").Id },
               new Discussion { Subject = "Prolog vs LISP", Description = "Diferente intre Prolog si LISP", course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id },
               new Discussion { Subject = "Texturi", Description = "La ce ne folosesc texturile", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Anaglife", Description = "La ce ne folosesc anaglifele", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Proiectii", Description = "Despre proiectii", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Transformari geometrice", Description = "Despre transformari geometrice", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Lumină şi umbră", Description = "Lumină şi umbră", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Grafica 2D", Description = "Ce este grafica 2D?", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Reprezentarea curbelor", Description = "Cum reprezentati curbel?", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Reprezentarea corpurilor", Description = "Cum reprezentati corpurile?", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Reprezentarea suprafetelor", Description = "Cum reprezentati suprafetele?", course_id = context.Courses.FirstOrDefault(p => p.Name == "Grafica pe calculator").Id },
               new Discussion { Subject = "Factory Method", Description = "About Factory Method", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Singleton", Description = "About Singleton", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Adapter", Description = "About Adapter", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Abstract Factory", Description = "About Abstract Factory", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Prototype", Description = "About Prototype", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Final project", Description = "About final project", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Mid term project", Description = "About mid term project", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Mid term project part 1", Description = "About mid term project", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Mid term project part 2", Description = "About mid term project", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "First assignment", Description = "About first assignment", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "Second assignment", Description = "About Second assignment", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id },
               new Discussion { Subject = "UML Diagrams", Description = "About UML Diagrams", course_id = context.Courses.FirstOrDefault(p => p.Name == "Design patterns").Id }

               );

            context.Questions.AddOrUpdate(
            new Question { Description = "Ce este FIP?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Gramatici si limbaje").Id },
            new Question { Description = "Ce este TS?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Gramatici si limbaje").Id },
            new Question { Description = "Ce este EF?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Conceptele bazelor de date").Id },
            new Question { Description = "Echivalentul 'break' in prolog", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },
            new Question { Description = "Ce inseamna CSS?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Introducere in CSS").Id },
            new Question { Description = "Ce reprezinta JQuery?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Introducere in JavaScript/JQuery").Id },
            new Question { Description = "Ce reprezinta : (Rnf )(x) = f (x) − (Tnf )(x) ?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Serii de numere reale").Id },
            new Question { Description = "Ce este IPC?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Procese si IPC in sistemele Windows.").Id },
            new Question { Description = "Un proces face parte dintr-un thread.", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Multiprocesare si concurenta folosind threaduri.").Id },
            new Question { Description = "Un thread face parte dintr-un proces.", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Recapitulare interfata socket()").Id },
            new Question { Description = "Ce este o tranzactie distribuita?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Gestiunea interblocarilor").Id },
            new Question { Description = "Ce ar trebui sa existe pentru a evita incalcarea 2PL?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Gestiunea interblocarilor").Id },
            new Question { Description = "Algorimtii de evitare a interblocarilor in BDD:", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Gestiunea interblocarilor").Id },
            new Question { Description = "Ce este o interogare?", test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Interogarea bazelor de date").Id }
);

        }

        private void seedPacket4(E_learningEntityContext context)
        {



            context.AnswerOptions.AddOrUpdate(


                new AnswerOptions { Description = "forma interna a programului", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este FIP?").Id, Correct = true },
                new AnswerOptions { Description = "forma iterativa a programului", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este FIP?").Id, Correct = false },
                new AnswerOptions { Description = "forma ilustrativa a programului", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este FIP?").Id, Correct = false },

                new AnswerOptions { Description = "tabela de simboluri", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este TS?").Id, Correct = true },
                new AnswerOptions { Description = "tabela de semne", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este TS?").Id, Correct = false },
                new AnswerOptions { Description = "tabela de semnale", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este TS?").Id, Correct = false },

                new AnswerOptions { Description = "entity framework", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este EF?").Id, Correct = true },
                new AnswerOptions { Description = "entity frames", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este EF?").Id, Correct = false },
                new AnswerOptions { Description = "entity form", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este EF?").Id, Correct = false },

                new AnswerOptions { Description = "!", question_id = context.Questions.FirstOrDefault(p => p.Description == "Echivalentul 'break' in prolog").Id, Correct = true },
                new AnswerOptions { Description = "%", question_id = context.Questions.FirstOrDefault(p => p.Description == "Echivalentul 'break' in prolog").Id, Correct = false },
                new AnswerOptions { Description = "$", question_id = context.Questions.FirstOrDefault(p => p.Description == "Echivalentul 'break' in prolog").Id, Correct = false },

                new AnswerOptions { Description = "cascading single sheet", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce inseamna CSS?").Id, Correct = false },
                new AnswerOptions { Description = "cascadin single style", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce inseamna CSS?").Id, Correct = false },
                new AnswerOptions { Description = "cascading style sheets", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce inseamna CSS?").Id, Correct = true },

                new AnswerOptions { Description = "un set de carti", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce reprezinta JQuery?").Id, Correct = false },
                new AnswerOptions { Description = "un set de librarii", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce reprezinta JQuery?").Id, Correct = false },
                new AnswerOptions { Description = "un set de librarii JavaScript", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce reprezinta JQuery?").Id, Correct = true },

                new AnswerOptions { Description = "Restul de ordinul x al formulei lui Taylor in punctul n", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce reprezinta : (Rnf )(x) = f (x) − (Tnf )(x) ?").Id, Correct = false },
                new AnswerOptions { Description = "Restul de ordinul n al formulei lui Taylor in punctul x", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce reprezinta : (Rnf )(x) = f (x) − (Tnf )(x) ?").Id, Correct = true },
                new AnswerOptions { Description = "Restul de ordinul x al formulei lui Taylor in punctul x", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce reprezinta : (Rnf )(x) = f (x) − (Tnf )(x) ?").Id, Correct = false },


                new AnswerOptions { Description = "inter process communication", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este IPC?").Id, Correct = true },
                new AnswerOptions { Description = "internal process communication", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este IPC?").Id, Correct = false },
                new AnswerOptions { Description = "intra process communication", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este IPC?").Id, Correct = false },


                new AnswerOptions { Description = "Fals", question_id = context.Questions.FirstOrDefault(p => p.Description == "Un proces face parte dintr-un thread.").Id, Correct = false },
                new AnswerOptions { Description = "Adevarat", question_id = context.Questions.FirstOrDefault(p => p.Description == "Un proces face parte dintr-un thread.").Id, Correct = true },


                new AnswerOptions { Description = "False", question_id = context.Questions.FirstOrDefault(p => p.Description == "Un thread face parte dintr-un proces.").Id, Correct = true },
                new AnswerOptions { Description = "Adevarat", question_id = context.Questions.FirstOrDefault(p => p.Description == "Un thread face parte dintr-un proces.").Id, Correct = false },


                new AnswerOptions { Description = "False", question_id = context.Questions.FirstOrDefault(p => p.Description == "Un thread face parte dintr-un proces.").Id, Correct = false },
                new AnswerOptions { Description = "Adevarat", question_id = context.Questions.FirstOrDefault(p => p.Description == "Un thread face parte dintr-un proces.").Id, Correct = true },


                new AnswerOptions { Description = "tranzactie care acceseaza fisiere de la diferite locatii ale unei BDD", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este o tranzactie distribuita?").Id, Correct = false },
                new AnswerOptions { Description = "tranzactie care acceseaza resurse de la aceleasi locatii ale unei BDD", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este o tranzactie distribuita?").Id, Correct = false },
                new AnswerOptions { Description = "tranzactie care acceseaza resurse de la diferite locatii ale unei BDD", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este o tranzactie distribuita?").Id, Correct = true },


                new AnswerOptions { Description = "sa aiba loc un schimb de mesaje intre locatii", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce ar trebui sa existe pentru a evita incalcarea 2PL?").Id, Correct = true },
                new AnswerOptions { Description = "sa aiba loc un schimb de mesaje intre fisiere", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce ar trebui sa existe pentru a evita incalcarea 2PL?").Id, Correct = false },
                new AnswerOptions { Description = "sa aiba loc un schimb de mesaje intre directoare", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce ar trebui sa existe pentru a evita incalcarea 2PL?").Id, Correct = false },




                new AnswerOptions { Description = "wait-death, wound-wait", question_id = context.Questions.FirstOrDefault(p => p.Description == "Algorimtii de evitare a interblocarilor in BDD:").Id, Correct = false },
                new AnswerOptions { Description = "wait-die, wound-wonder", question_id = context.Questions.FirstOrDefault(p => p.Description == "Algorimtii de evitare a interblocarilor in BDD:").Id, Correct = false },
                new AnswerOptions { Description = "wait-die, wound-wait", question_id = context.Questions.FirstOrDefault(p => p.Description == "Algorimtii de evitare a interblocarilor in BDD:").Id, Correct = true },


                new AnswerOptions { Description = "operatia prin care se obtin directoare dorite dintr-o baza de date", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este o interogare?").Id, Correct = false },
                new AnswerOptions { Description = "operatia prin care se obtin datele dorite dintr-o baza de date", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este o interogare?").Id, Correct = true },
                new AnswerOptions { Description = "operatia prin care se obtin fisiere dorite dintr-o baza de date", question_id = context.Questions.FirstOrDefault(p => p.Description == "Ce este o interogare?").Id, Correct = false }





                );


            context.Homeworks.AddOrUpdate(
             new Homework { Name = "Tema Limbaje regulare", Description = "Tema Limbaje regulare", Format = "scris", End_date = DateTime.Now, Grading_type = "online", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Limbaje regulare").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },


             new Homework { Name = "Tema CSS", Description = "Write a web page which displays a number of images in a table. The table should be formatted using a style sheet. Also when the mouse is over an image, the image should increase opacity with a specific amount.", Format = "", End_date = DateTime.Today, Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in CSS").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },


             new Homework { Name = "Tema Prolog", Description = "Definiti un predicat care determina succesorul unui numar reprezentat cifra cu cifra intr-o lista. De ex: [1 9 3 5 9 9] --> [1 9 3 6 0 0] ", Format = "", End_date = new DateTime(2015, 9, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Programul Prolog").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },


             new Homework { Name = "Tema HTML", Description = "Sa se scrie o pagina web ce contine un formular utilizat în viata reala.", Format = "", End_date = new DateTime(2015, 10, 14), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in HTML. Structura unui document HTML.Taguri de baza.").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },


             new Homework { Name = "Tema JavaScript", Description = "Sa se scrie un formular care utilizeaza o functie JavaScript care valideaza formatul unei adrese de email. Adresa de email validata trebuie sa fie in formatul specificat in RFC 822 si RFC 2822.", Format = "", End_date = new DateTime(2015, 10, 21), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in JavaScript/JQuery").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },


             new Homework { Name = "Tema JQuery", Description = "Folosind jQuery, la selectarea prin dubluclick a unui cuvant din cadrul paginii, sa se coloreze intr-o anumita culoare pe un anumit fond de alta culoare toate aparitiile cuvantului respectiv in cadrul paginii. Nu se vor folosi alte biblioteci de functii, pluginuri, etc in afara de jQuery (jquery.min.js).", Format = "", End_date = new DateTime(2015, 11, 10), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in JavaScript/JQuery").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },


             new Homework { Name = "Tema Limbaje regulare", Description = "", Format = "", End_date = new DateTime(2015, 9, 21), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Limbaje regulare").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },


             new Homework { Name = "Tema CSS", Description = "Write a web page which displays a number of images in a table. The table should be formatted using a style sheet. Also when the mouse is over an image, the image should increase opacity with a specific amount.", Format = "", End_date = new DateTime(2015, 9, 14), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in CSS").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },


             new Homework { Name = "Tema Prolog", Description = "Definiti un predicat care determina succesorul unui numar reprezentat cifra cu cifra intr-o lista. De ex: [1 9 3 5 9 9] --> [1 9 3 6 0 0] ", Format = "", End_date = new DateTime(2015, 10, 19), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Programul Prolog").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },


             new Homework { Name = "Tema HTML", Description = "Sa se scrie o pagina web ce contine un formular utilizat în viata reala.", Format = "", End_date = new DateTime(2015, 11, 26), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in HTML. Structura unui document HTML.Taguri de baza.").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },


             new Homework { Name = "Tema JavaScript", Description = "Sa se scrie un formular care utilizeaza o functie JavaScript care valideaza formatul unei adrese de email. Adresa de email validata trebuie sa fie in formatul specificat in RFC 822 si RFC 2822.", Format = "", End_date = new DateTime(2015, 9, 27), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in JavaScript/JQuery").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },


             new Homework { Name = "Tema JQuery", Description = "Folosind jQuery, la selectarea prin dubluclick a unui cuvant din cadrul paginii, sa se coloreze intr-o anumita culoare pe un anumit fond de alta culoare toate aparitiile cuvantului respectiv in cadrul paginii. Nu se vor folosi alte biblioteci de functii, pluginuri, etc in afara de jQuery (jquery.min.js).", Format = "", End_date = new DateTime(2015, 12, 12), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Introducere in JavaScript/JQuery").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },

             new Homework { Name = "Tema socket", Description = "Un client trimite unui server un sir de numere. Serverul va returna clientului suma numerelor primite.", Format = "", End_date = new DateTime(2015, 12, 14), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Recapitulare interfata socket()").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },

             new Homework { Name = "Tema protocoale", Description = "Un client trimite unui server un sir de lugime cel mult 100 de caractere. Serverul va returna clientului numarul de caractere spatiu din sir.", Format = "", End_date = new DateTime(2015, 12, 16), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Notiunea de protocol. Stive de protocoale. Modelul OSI si TCP / IP.").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },

             new Homework { Name = "Tema socket", Description = "Un client trimite unui server un sir de numere. Serverul va returna clientului suma numerelor primite.", Format = "", End_date = new DateTime(2015, 12, 2), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Recapitulare interfata socket()").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },

             new Homework { Name = "Tema protocoale", Description = "Un client trimite unui server un sir de lugime cel mult 100 de caractere. Serverul va returna clientului numarul de caractere spatiu din sir.", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Notiunea de protocol. Stive de protocoale. Modelul OSI si TCP / IP.").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },

             new Homework { Name = "Tema grafica 2D", Description = "Să se realizeze o aplicaţie practică pentru o problemă (la alegere) de analiză matematică sau de geometrie plană(de exemplu o problemă de loc geometric) cu reprezentare grafică şi interpretarea rezultatelor de pe ecran cu ajutorul mouse - lui.", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Grafica 2D").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },

             new Homework { Name = "Tema grafica 2D", Description = "Să se realizeze o aplicaţie practică pentru o problemă (la alegere) de analiză matematică sau de geometrie plană(de exemplu o problemă de loc geometric) cu reprezentare grafică şi interpretarea rezultatelor de pe ecran cu ajutorul mouse - lui.", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Grafica 2D").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },

             new Homework { Name = "Tema grafica 2D", Description = "Să se realizeze o aplicaţie pentru a desena o histogramă a temperaturilor medii lunare dintr - un an(realizată cu dreptunghiuri).", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Grafica 2D").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },

             new Homework { Name = "Tema grafica 2D", Description = "Să se realizeze o aplicaţie pentru a desena o histogramă a temperaturilor medii lunare dintr - un an(realizată cu dreptunghiuri).", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Grafica 2D").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id },

             new Homework { Name = "Tema grafica 3D", Description = "Să se reprezinte cel puţin un obiect 3D:curbă, suprafaţă[de rotaţie], corp < prin muchii / feţe", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Reprezentarea corpurilor").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "235").Id },

             new Homework { Name = "Tema grafica 3D", Description = "Să se reprezinte cel puţin un obiect 3D:curbă, suprafaţă[de rotaţie], corp < prin muchii / feţe", Format = "", End_date = new DateTime(2015, 11, 29), Grading_type = "", Required = true, module_id = context.Modules.FirstOrDefault(p => p.Name == "Reprezentarea corpurilor").Id, group_id = context.Groups.FirstOrDefault(p => p.Name == "236").Id }


);

        }

        private void seedPacket5(E_learningEntityContext context)
        {
            context.Comments.AddOrUpdate(


new Models.Comment
{

    Message = "http://www.w3schools.com/jquery/",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Libraria JQuery").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 9, 18)
},

new Models.Comment
{
    Message = "http://blog.samibadawi.com/2013/05/lisp-prolog-and-evolution.html",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "andic").Id,
    Date = new DateTime(2015, 10, 22)
},


new Models.Comment
{
    Message = "http://stackoverflow.com/questions/5224524/lisp-and-prolog-for-artificial-intelligence",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 11, 15)
},


new Models.Comment
{
    Message = "http://stackoverflow.com/questions/5224524/lisp-and-prolog-for-artificial-intelligence",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 11, 15)
},

new Models.Comment
{
    Message = "C#, SQL",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Proiect Colectiv").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "elizan").Id,
    Date = new DateTime(2015, 11, 15)
},

new Models.Comment
{
    Message = "Sa ne spuna Oana ",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Proiect Colectiv").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "cristip").Id,
    Date = new DateTime(2015, 11, 16)
},



new Models.Comment
{
    Message = "Java,etc.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Proiect Colectiv").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 11, 16)
},


new Models.Comment
{
    Message = "Se gasesc majoritatea pe site-u’ lui bufny.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Practic").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id,
    Date = new DateTime(2015, 11, 16)
},
new Models.Comment
{
    Message = "Si pe w3school",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Practic").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "cerbu").Id,
    Date = new DateTime(2015, 11, 16)
},


new Models.Comment
{
    Message = "Caut arhiva cu modele de anii trecuti.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Exercitii Analiza matematica").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id,
    Date = new DateTime(2015, 11, 22)
},

new Models.Comment
{
    Message = " https://www.wetransfer.com/downloads/e906f4b62e1399f09abda9885487c85220160115190630/8e2e3f",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Exercitii Analiza matematica").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "alexandras").Id,
    Date = new DateTime(2015, 11, 22)
},

new Models.Comment
{
    Message = "Multumiiim",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Exercitii Analiza matematica").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 11, 22)
},





new Models.Comment
{
    Message = "Ne putem intalni intr-o zi saptamana aceasta sau urmatoare pentru consultatii?",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Recapitulare").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "benim").Id,
    Date = new DateTime(2015, 12, 12)
},
new Models.Comment
{
    Message = "Da. O sa caut sala si va anunt",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Recapitulare").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "ancaa").Id,
    Date = new DateTime(2015, 12, 12)
},
new Models.Comment
{
    Message = " https://docs.oracle.com/javase/tutorial/networking/sockets/",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Socket-uri").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "cristianas").Id,
    Date = new DateTime(2015, 11, 12)
},
new Models.Comment
{
    Message = "Multumim, Cristiana !",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Socket-uri").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "bradu").Id,
    Date = new DateTime(2015, 11, 12)
},
new Models.Comment
{
    Message = "Va rog sa cititi link-ul postat de Cristiana. O sa va intreb la urmatorul laborator din el.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Socket-uri").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "bradu").Id,
    Date = new DateTime(2015, 11, 12)
},
new Models.Comment
{
    Message = "task managerul trimite o cere de blocare planificatorului. Daca o operatie e in conflict cu o blocare atunci va fi amanata. Operatiile conflictuale sunt executate in ordinea in care sunt obtinute blocarile corespunzatoare conflictuale.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Interblocari").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id,
    Date = new DateTime(2015, 11, 12)
},
new Models.Comment
{
    Message = "Corect.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Interblocari").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "andreean").Id,
    Date = new DateTime(2015, 11, 12)
},

        new Models.Comment
        {
            Message = "Entity Framework enables developers to create data access applications by programming against a conceptual application model instead of programming directly against a relational storage schema. The goal is to decrease the amount of code and maintenance required for data-oriented applications.",
            discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "EntityFramework").Id,
            user_id = context.Users.FirstOrDefault(p => p.Username == "ancas").Id,
            Date = new DateTime(2015, 10, 12)
        },

new Models.Comment
{
    Message = "Google sa ne aiba in paza.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "EntityFramework").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "roxanar").Id,
    Date = new DateTime(2015, 10, 12)
},
new Models.Comment
{
    Message = "Regulate sau neregulate?",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Texturi").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 11, 22)
},
new Models.Comment
{
    Message = "Pentru inceput cele regulate.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Texturi").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "per").Id,
    Date = new DateTime(2015, 11, 13)
},
new Models.Comment
{
    Message = "Ne ajuta ca sa punem(asezam) caractere peste un obicet",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Texturi").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2015, 11, 13)
},
new Models.Comment
{
    Message = "obiect* XD",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Texturi").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id,
    Date = new DateTime(2015, 11, 13)
},
new Models.Comment
{
    Message = "Mai din topor spus, dar sa zicem ca e bine.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Texturi").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "per").Id,
    Date = new DateTime(2015, 11, 14)
},
new Models.Comment
{
    Message = "Sunt folosite la grafica 3D.",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Anaglife").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "arianap").Id,
    Date = new DateTime(2015, 11, 13)
},
new Models.Comment
{
    Message = "Ce este foarte important in legatura cu anaglifele?",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Anaglife").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "per").Id,
    Date = new DateTime(2015, 11, 14)
},
new Models.Comment
{
    Message = "Alegerea nuantelor?",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Anaglife").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id,
    Date = new DateTime(2015, 11, 14)
},
new Models.Comment
{
    Message = "Da",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Anaglife").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "per").Id,
    Date = new DateTime(2015, 11, 14)
},
new Models.Comment
{
    Message = "https://en.wikipedia.org/wiki/Singleton_pattern",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Singleton").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id,
    Date = new DateTime(2016, 01, 14)
},
new Models.Comment
{
    Message = "http://www.tutorialspoint.com/design_pattern/singleton_pattern.htm     util :)",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Anaglife").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "biancap").Id,
    Date = new DateTime(2016, 01, 14)
},
new Models.Comment
{
    Message = "http://www.tutorialspoint.com/design_pattern/prototype_pattern.htm",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prototype").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "larisaz").Id,
    Date = new DateTime(2016, 01, 14)
},
new Models.Comment
{
    Message = "wow! Multumim Larisa",
    discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prototype").Id,
    user_id = context.Users.FirstOrDefault(p => p.Username == "cerbu").Id,
    Date = new DateTime(2016, 01, 14)
}

);
        }


        private void seedPacket6(E_learningEntityContext context)
        {
            context.Events.AddOrUpdate(
               new Models.Event { Name = "Laborator 1 lftc", Description = "Laborator 1 lftc", End_date = new DateTime(2015, 9, 20), user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
               new Models.Event { Name = "Laborator 2 lftc", Description = "Laborator 2 lftc", End_date = new DateTime(2015, 10, 2), user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
               new Models.Event { Name = "Laborator 3 lftc", Description = "Laborator 3 lftc", End_date = new DateTime(2015, 10, 20), user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
               new Models.Event { Name = "Laborator 4 lftc", Description = "Laborator 4 lftc", End_date = new DateTime(2015, 10, 30), user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },

               new Models.Event { Name = "Laborator 1 lftc", Description = "Laborator 1 lftc", End_date = new DateTime(2015, 9, 20), user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id },
               new Models.Event { Name = "Laborator 2 lftc", Description = "Laborator 2 lftc", End_date = new DateTime(2015, 10, 2), user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id },
               new Models.Event { Name = "Laborator 3 lftc", Description = "Laborator 3 lftc", End_date = new DateTime(2015, 10, 20), user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id },
               new Models.Event { Name = "Laborator 4 lftc", Description = "Laborator 4 lftc", End_date = new DateTime(2015, 10, 30), user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id },

               new Models.Event { Name = "Laborator 1 lftc", Description = "Laborator 1 lftc", End_date = new DateTime(2015, 9, 20), user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
               new Models.Event { Name = "Laborator 2 lftc", Description = "Laborator 2 lftc", End_date = new DateTime(2015, 10, 2), user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
               new Models.Event { Name = "Laborator 3 lftc", Description = "Laborator 3 lftc", End_date = new DateTime(2015, 10, 20), user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
               new Models.Event { Name = "Laborator 4 lftc", Description = "Laborator 4 lftc", End_date = new DateTime(2015, 10, 30), user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id }

               );
            context.Grades.AddOrUpdate(

                new Grade { Value = 9, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Libraria JQuery").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare web").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema JQuery").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Introducere in JavaScript/JQuery").Id },

new Grade { Value = 10, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 7, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 10, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 6, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 5, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 9, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 9, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id },

new Grade { Value = 10, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, discussion_id = context.Discussions.FirstOrDefault(p => p.Subject == "Prolog vs LISP").Id, course_id = context.Courses.FirstOrDefault(p => p.Name == "Programare logica si functionala").Id, homework_id = context.Homeworks.FirstOrDefault(p => p.Name == "Tema Prolog").Id, test_id = context.Tests.FirstOrDefault(p => p.Name == "Test Programul Prolog").Id }
                );
            context.Conversations.AddOrUpdate(
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { },
                new Conversation { }


                );
        }

        private void seedPacket7(E_learningEntityContext context)
        {
            //            context.Logs.AddOrUpdate(
            //              new Logs { Date = DateTime.Today, Type = "t1", user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
            //new Logs { Date = DateTime.Today, Type = "t1", user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id },
            //new Logs { Date = DateTime.Today, Type = "t1", user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
            //new Logs { Date = DateTime.Today, Type = "t2", user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
            //new Logs { Date = DateTime.Today, Type = "t2", user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id },
            //new Logs { Date = DateTime.Today, Type = "t2", user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id },
            //new Logs { Date = DateTime.Today, Type = "t3", user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
            //new Logs { Date = DateTime.Today, Type = "t3", user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id },
            //new Logs { Date = DateTime.Today, Type = "t3", user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id }
            //              );

            context.UserConversations.AddOrUpdate(
                new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 9).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 7).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 7).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 8).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 8).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 9).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 9).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 3).Id },
new UserConversation { user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, conversation_id = context.Conversations.FirstOrDefault(p => p.Id == 3).Id }

                );
        }
        private void seedPacket8(E_learningEntityContext context)
        {
            context.Messages.AddOrUpdate(
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 5).Id, Date = DateTime.Now },
new Message { Text = "buna!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 5).Id, Date = DateTime.Now },
new Message { Text = "Ai fost la ultimul laborator de programare web?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 5).Id, Date = DateTime.Now },
new Message { Text = "Da", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 7).Id, Date = DateTime.Now },
new Message { Text = "S-au impartit temele? Nu am vazut sa se updateze ceva pe site...", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 7).Id, Date = DateTime.Now },
new Message { Text = "Da. Se vor updata in decursul zilei.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 7).Id, Date = DateTime.Now },
new Message { Text = "Ok. Multumesc!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 8).Id, Date = DateTime.Now },
new Message { Text = "Nu ai pentru ce smile emoticon", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 8).Id, Date = DateTime.Now },

new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 8).Id, Date = DateTime.Now },
new Message { Text = "Ii poti spune profului de analiza ca nu pot ajunge la seminar. O sa recuperez cu o alta grupa. I-am trimis mail, dar nu a raspuns.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Sigur :)", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 8).Id, Date = DateTime.Now },
new Message { Text = "Mersi! Esti un prieten de nadejde!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 8).Id, Date = DateTime.Now },


new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Tie trebuie sa iti dau banii pentru festivitate?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Dada", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ne putem intalni maine pe la 13:50?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "La FSEGA?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Da. La etajul 3 la lifturi.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "In regula. Ne vedem maine :)", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ok :) salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },


new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Poti sa ma treci si pe mine pentru festivitate, dar la banchet nu pot veni.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ok.. imi pare rau :(", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Petrecem cu alta ocazie ;)", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },


new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Maine recuperam seminarul de LFTC?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Da. La drept, la ultimul etaj, unde faceam seminarul de Analiza", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Super! Mersi :)", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "N-ai de ce! Ne vedem maine :)", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },

new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Nu ai idee cand se recupereaza laboratoarele pierdute de la web?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Stai sa imi verific mail-ul", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Sigur", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Nu mi-a mai trimis nimic profa", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ook", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Te anunt daca aflu ceva. O sa pun si pe grup", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Mersi :)", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },


new Message { Text = "Salut! Sa va uitati pe grupul nostru. V-am scris legat de banchet si festivitatea de premiere. Trebuie pana luni sa imi spuneti daca veniti sau nu.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ok. O sa te anunt", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },

new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ai reusit sa faci vreun laborator la Design?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Nu... ma pierd la materia asta", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Da.. La fel si eu.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Oricum a zis ca laboratoarele le noteaza la final de semestru..pana atunci avem timp sa le facem si sa le modificam", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Sa speram ca avem ceva de modificat pana atunci :))", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ne descurcam noi!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ca intotdeauna", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },



new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Salut!", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Pana la urma la BD s-a stabilit ceva cu profu? Mai avem timp de predat a 3-a faza?", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Nu am stabilit nimic. Eventual trimite-i mail sa vezi ce zice. Din cate stiu e ok sa mergi si cu alte grupe. Daca are ore sapt asta mergi fara griji.", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Ok. Mersi! :) ", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now },
new Message { Text = "Nu ai de ce", userConversation_id = context.UserConversations.FirstOrDefault(p => p.Id == 9).Id, Date = DateTime.Now }

);


            //            context.UserNotifications.AddOrUpdate(
            //               new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n1").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n1").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n1").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n2").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n2").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n2").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n3").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n3").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n3").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n4").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n4").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n4").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanap").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n5").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "oanam").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n5").Id },
            //new UserNotification { Seen = true, user_id = context.Users.FirstOrDefault(p => p.Username == "alexr").Id, notification_id = context.Notifications.FirstOrDefault(p => p.Message == "n5").Id }


            //            //                );

        }


    }

}
