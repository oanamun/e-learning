namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Completed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Groups", "course_id", c => c.Int());
            AddColumn("dbo.Tests", "Completed", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Groups", "course_id");
            AddForeignKey("dbo.Groups", "course_id", "dbo.Courses", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Groups", "course_id", "dbo.Courses");
            DropIndex("dbo.Groups", new[] { "course_id" });
            DropColumn("dbo.Tests", "Completed");
            DropColumn("dbo.Groups", "course_id");
        }
    }
}
