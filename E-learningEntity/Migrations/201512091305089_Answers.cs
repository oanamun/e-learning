namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Answers : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Answers", newName: "AnswerOptions");
            CreateTable(
                "dbo.UserAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        user_Id = c.Int(nullable: false),
                        answerOptionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AnswerOptions", t => t.answerOptionId)
                .ForeignKey("dbo.Users", t => t.user_Id)
                .Index(t => t.user_Id)
                .Index(t => t.answerOptionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserAnswers", "user_Id", "dbo.Users");
            DropForeignKey("dbo.UserAnswers", "answerOptionId", "dbo.AnswerOptions");
            DropIndex("dbo.UserAnswers", new[] { "answerOptionId" });
            DropIndex("dbo.UserAnswers", new[] { "user_Id" });
            DropTable("dbo.UserAnswers");
            RenameTable(name: "dbo.AnswerOptions", newName: "Answers");
        }
    }
}
