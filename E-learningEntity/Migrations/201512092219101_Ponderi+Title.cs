namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PonderiTitle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discussions", "Pondere", c => c.Single());
            AddColumn("dbo.Homework", "Pondere", c => c.Single());
            AddColumn("dbo.Pages", "Title", c => c.String());
            AddColumn("dbo.Tests", "Pondere", c => c.Single());
            AddColumn("dbo.Questions", "Pondere", c => c.Single());
            DropColumn("dbo.Grades", "Pondere");
            DropColumn("dbo.Pages", "Titlu");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pages", "Titlu", c => c.String());
            AddColumn("dbo.Grades", "Pondere", c => c.Single(nullable: false));
            DropColumn("dbo.Questions", "Pondere");
            DropColumn("dbo.Tests", "Pondere");
            DropColumn("dbo.Pages", "Title");
            DropColumn("dbo.Homework", "Pondere");
            DropColumn("dbo.Discussions", "Pondere");
        }
    }
}
