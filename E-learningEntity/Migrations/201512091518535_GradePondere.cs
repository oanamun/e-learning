namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GradePondere : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Grades", "Pondere", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Grades", "Pondere");
        }
    }
}
