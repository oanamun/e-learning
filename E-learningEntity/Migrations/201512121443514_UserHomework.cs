namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserHomework : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserHomeworks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Answer = c.String(),
                        homework_id = c.Int(nullable: false),
                        user_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Homework", t => t.homework_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.homework_id)
                .Index(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserHomeworks", "user_id", "dbo.Users");
            DropForeignKey("dbo.UserHomeworks", "homework_id", "dbo.Homework");
            DropIndex("dbo.UserHomeworks", new[] { "user_id" });
            DropIndex("dbo.UserHomeworks", new[] { "homework_id" });
            DropTable("dbo.UserHomeworks");
        }
    }
}
