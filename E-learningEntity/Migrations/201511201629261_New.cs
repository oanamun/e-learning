namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Announcements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Message = c.String(),
                        course_id = c.Int(),
                        group_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.course_id)
                .ForeignKey("dbo.Groups", t => t.group_id)
                .Index(t => t.course_id)
                .Index(t => t.group_id);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Department = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false),
                        Description = c.String(),
                        Category = c.String(),
                        course_id = c.Int(),
                        group_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.course_id)
                .ForeignKey("dbo.Groups", t => t.group_id)
                .Index(t => t.course_id)
                .Index(t => t.group_id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(nullable: false),
                        discussion_id = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Discussions", t => t.discussion_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.discussion_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        First_name = c.String(),
                        Last_name = c.String(),
                        Email = c.String(),
                        Registration_number = c.Int(nullable: false),
                        Suspended = c.Boolean(nullable: false),
                        Blocked = c.Boolean(nullable: false),
                        SuspendedUntil = c.DateTime(),
                        group_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.group_id)
                .Index(t => t.group_id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Start_date = c.DateTime(nullable: false),
                        End_date = c.DateTime(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                        discussion_id = c.Int(),
                        course_id = c.Int(),
                        homework_id = c.Int(),
                        test_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.course_id)
                .ForeignKey("dbo.Discussions", t => t.discussion_id)
                .ForeignKey("dbo.Homework", t => t.homework_id)
                .ForeignKey("dbo.Tests", t => t.test_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id)
                .Index(t => t.discussion_id)
                .Index(t => t.course_id)
                .Index(t => t.homework_id)
                .Index(t => t.test_id);
            
            CreateTable(
                "dbo.Homework",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Score = c.String(),
                        Format = c.String(),
                        Start_date = c.DateTime(nullable: false),
                        End_date = c.DateTime(),
                        Grading_type = c.String(),
                        Required = c.Boolean(nullable: false),
                        module_id = c.Int(),
                        group_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.group_id)
                .ForeignKey("dbo.Modules", t => t.module_id)
                .Index(t => t.module_id)
                .Index(t => t.group_id);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        group_id = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.group_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.group_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Due_date = c.DateTime(),
                        Type = c.String(),
                        module_id = c.Int(),
                        group_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.group_id)
                .ForeignKey("dbo.Modules", t => t.module_id)
                .Index(t => t.module_id)
                .Index(t => t.group_id);
            
            CreateTable(
                "dbo.Modules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Start_date = c.DateTime(),
                        course_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.course_id)
                .Index(t => t.course_id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Answer = c.String(),
                        Score = c.Int(nullable: false),
                        test_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tests", t => t.test_id)
                .Index(t => t.test_id);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Type = c.String(),
                        user_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Role = c.String(nullable: false),
                        user_id = c.Int(nullable: false),
                        course_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.course_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id)
                .Index(t => t.course_id);
            
            CreateTable(
                "dbo.UserConversations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        user_id = c.Int(nullable: false),
                        conversation_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Conversations", t => t.conversation_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id)
                .Index(t => t.conversation_id);
            
            CreateTable(
                "dbo.Conversations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        userConversation_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserConversations", t => t.userConversation_id)
                .Index(t => t.userConversation_id);
            
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Seen = c.Boolean(nullable: false),
                        user_id = c.Int(nullable: false),
                        notification_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notifications", t => t.notification_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id)
                .Index(t => t.notification_id);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tokens",
                c => new
                    {
                        TokenId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        AuthToken = c.String(),
                        IssuedOn = c.DateTime(nullable: false),
                        ExpiresOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TokenId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tokens", "UserId", "dbo.Users");
            DropForeignKey("dbo.Announcements", "group_id", "dbo.Groups");
            DropForeignKey("dbo.Announcements", "course_id", "dbo.Courses");
            DropForeignKey("dbo.Discussions", "group_id", "dbo.Groups");
            DropForeignKey("dbo.Discussions", "course_id", "dbo.Courses");
            DropForeignKey("dbo.Comments", "user_id", "dbo.Users");
            DropForeignKey("dbo.UserNotifications", "user_id", "dbo.Users");
            DropForeignKey("dbo.UserNotifications", "notification_id", "dbo.Notifications");
            DropForeignKey("dbo.UserConversations", "user_id", "dbo.Users");
            DropForeignKey("dbo.Messages", "userConversation_id", "dbo.UserConversations");
            DropForeignKey("dbo.UserConversations", "conversation_id", "dbo.Conversations");
            DropForeignKey("dbo.Permissions", "user_id", "dbo.Users");
            DropForeignKey("dbo.Permissions", "course_id", "dbo.Courses");
            DropForeignKey("dbo.Logs", "user_id", "dbo.Users");
            DropForeignKey("dbo.Users", "group_id", "dbo.Groups");
            DropForeignKey("dbo.Grades", "user_id", "dbo.Users");
            DropForeignKey("dbo.Grades", "test_id", "dbo.Tests");
            DropForeignKey("dbo.Grades", "homework_id", "dbo.Homework");
            DropForeignKey("dbo.Homework", "module_id", "dbo.Modules");
            DropForeignKey("dbo.Homework", "group_id", "dbo.Groups");
            DropForeignKey("dbo.Questions", "test_id", "dbo.Tests");
            DropForeignKey("dbo.Tests", "module_id", "dbo.Modules");
            DropForeignKey("dbo.Modules", "course_id", "dbo.Courses");
            DropForeignKey("dbo.Tests", "group_id", "dbo.Groups");
            DropForeignKey("dbo.Pages", "user_id", "dbo.Users");
            DropForeignKey("dbo.Pages", "group_id", "dbo.Groups");
            DropForeignKey("dbo.Grades", "discussion_id", "dbo.Discussions");
            DropForeignKey("dbo.Grades", "course_id", "dbo.Courses");
            DropForeignKey("dbo.Events", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "discussion_id", "dbo.Discussions");
            DropIndex("dbo.Tokens", new[] { "UserId" });
            DropIndex("dbo.UserNotifications", new[] { "notification_id" });
            DropIndex("dbo.UserNotifications", new[] { "user_id" });
            DropIndex("dbo.Messages", new[] { "userConversation_id" });
            DropIndex("dbo.UserConversations", new[] { "conversation_id" });
            DropIndex("dbo.UserConversations", new[] { "user_id" });
            DropIndex("dbo.Permissions", new[] { "course_id" });
            DropIndex("dbo.Permissions", new[] { "user_id" });
            DropIndex("dbo.Logs", new[] { "user_id" });
            DropIndex("dbo.Questions", new[] { "test_id" });
            DropIndex("dbo.Modules", new[] { "course_id" });
            DropIndex("dbo.Tests", new[] { "group_id" });
            DropIndex("dbo.Tests", new[] { "module_id" });
            DropIndex("dbo.Pages", new[] { "user_id" });
            DropIndex("dbo.Pages", new[] { "group_id" });
            DropIndex("dbo.Homework", new[] { "group_id" });
            DropIndex("dbo.Homework", new[] { "module_id" });
            DropIndex("dbo.Grades", new[] { "test_id" });
            DropIndex("dbo.Grades", new[] { "homework_id" });
            DropIndex("dbo.Grades", new[] { "course_id" });
            DropIndex("dbo.Grades", new[] { "discussion_id" });
            DropIndex("dbo.Grades", new[] { "user_id" });
            DropIndex("dbo.Events", new[] { "User_Id" });
            DropIndex("dbo.Users", new[] { "group_id" });
            DropIndex("dbo.Comments", new[] { "user_id" });
            DropIndex("dbo.Comments", new[] { "discussion_id" });
            DropIndex("dbo.Discussions", new[] { "group_id" });
            DropIndex("dbo.Discussions", new[] { "course_id" });
            DropIndex("dbo.Announcements", new[] { "group_id" });
            DropIndex("dbo.Announcements", new[] { "course_id" });
            DropTable("dbo.Tokens");
            DropTable("dbo.Notifications");
            DropTable("dbo.UserNotifications");
            DropTable("dbo.Messages");
            DropTable("dbo.Conversations");
            DropTable("dbo.UserConversations");
            DropTable("dbo.Permissions");
            DropTable("dbo.Logs");
            DropTable("dbo.Questions");
            DropTable("dbo.Modules");
            DropTable("dbo.Tests");
            DropTable("dbo.Pages");
            DropTable("dbo.Groups");
            DropTable("dbo.Homework");
            DropTable("dbo.Grades");
            DropTable("dbo.Events");
            DropTable("dbo.Users");
            DropTable("dbo.Comments");
            DropTable("dbo.Discussions");
            DropTable("dbo.Courses");
            DropTable("dbo.Announcements");
        }
    }
}
