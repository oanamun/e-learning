﻿using E_learningEntity.Models;
using E_learningEntity.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace E_learningEntity.ActionFilters
{
    public class CourseTeacherAuthorizationRequired : ActionFilterAttribute
    {
        private int courseId = 0;
        private const string Token = "Token";

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //  Get API key provider
            var provider = filterContext.ControllerContext.Configuration
            .DependencyResolver.GetService(typeof(ITokenServices)) as ITokenServices;

            if (filterContext.Request.Headers.Contains(Token))
            {
                var tokenValue = filterContext.Request.Headers.GetValues(Token).First();
                //daca nu are course, atunci cautam courseId
                try
                {
                    Course course = (Course)filterContext.ActionArguments["course"];
                    courseId = course.Id;
                }catch(Exception){
                    courseId=(int)filterContext.ActionArguments["courseId"];
                }
                if (courseId != 0)
                {
                    if (provider == null || !provider.ValidateToken(tokenValue) || !provider.ValidateTokenTeacherForCourse(tokenValue, courseId))
                    {
                        var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                        filterContext.Response = responseMessage;
                    }
                }
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}